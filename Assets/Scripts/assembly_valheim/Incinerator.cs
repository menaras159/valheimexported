using UnityEngine;
using System;
using System.Collections.Generic;

public class Incinerator : MonoBehaviour
{
	[Serializable]
	public class Requirement
	{
		public ItemDrop m_resItem;
		public int m_amount;
	}

	[Serializable]
	public class IncineratorConversion
	{
		public List<Incinerator.Requirement> m_requirements;
		public ItemDrop m_result;
		public int m_resultAmount;
		public int m_priority;
		public bool m_requireOnlyOneIngredient;
	}

	public Switch m_incinerateSwitch;
	public Container m_container;
	public Animator m_leverAnim;
	public GameObject m_lightingAOEs;
	public EffectList m_leverEffects;
	public float m_effectDelayMin;
	public float m_effectDelayMax;
	public List<Incinerator.IncineratorConversion> m_conversions;
	public ItemDrop m_defaultResult;
	public int m_defaultCost;
}
