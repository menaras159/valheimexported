using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
	public enum Type
	{
		Player = 0,
		Camera = 1,
	}

	public Type m_follow;
	public bool m_lockYPos;
	public bool m_followCameraInFreefly;
	public float m_maxYPos;
}
