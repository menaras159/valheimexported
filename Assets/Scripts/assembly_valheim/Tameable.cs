using UnityEngine;

public class Tameable : MonoBehaviour
{
	public float m_fedDuration;
	public float m_tamingTime;
	public EffectList m_tamedEffect;
	public EffectList m_sootheEffect;
	public EffectList m_petEffect;
	public bool m_commandable;
	public ItemDrop m_saddleItem;
	public Sadle m_saddle;
	public bool m_dropSaddleOnDeath;
	public Vector3 m_dropSaddleOffset;
	public float m_dropItemVel;
}
