using UnityEngine;
using System;
using System.Collections.Generic;

public class Smelter : MonoBehaviour
{
	[Serializable]
	public class ItemConversion
	{
		public ItemDrop m_from;
		public ItemDrop m_to;
	}

	public string m_name;
	public string m_addOreTooltip;
	public string m_emptyOreTooltip;
	public Switch m_addWoodSwitch;
	public Switch m_addOreSwitch;
	public Switch m_emptyOreSwitch;
	public Transform m_outputPoint;
	public Transform m_roofCheckPoint;
	public GameObject m_enabledObject;
	public GameObject m_disabledObject;
	public GameObject m_haveFuelObject;
	public GameObject m_haveOreObject;
	public GameObject m_noOreObject;
	public Animator[] m_animators;
	public ItemDrop m_fuelItem;
	public int m_maxOre;
	public int m_maxFuel;
	public int m_fuelPerProduct;
	public float m_secPerProduct;
	public bool m_spawnStack;
	public bool m_requiresRoof;
	public Windmill m_windmill;
	public SmokeSpawner m_smokeSpawner;
	public List<Smelter.ItemConversion> m_conversion;
	public EffectList m_oreAddedEffects;
	public EffectList m_fuelAddedEffects;
	public EffectList m_produceEffects;
}
