using UnityEngine;

public class Floating : MonoBehaviour
{
	public float m_waterLevelOffset;
	public float m_forceDistance;
	public float m_force;
	public float m_balanceForceFraction;
	public float m_damping;
	public EffectList m_impactEffects;
	public GameObject m_surfaceEffects;
}
