using System;
using UnityEngine;

public class HitData
{
	[Serializable]
	public struct DamageTypes
	{
		public float m_damage;
		public float m_blunt;
		public float m_slash;
		public float m_pierce;
		public float m_chop;
		public float m_pickaxe;
		public float m_fire;
		public float m_frost;
		public float m_lightning;
		public float m_poison;
		public float m_spirit;
	}

	[Serializable]
	public struct DamageModPair
	{
		public HitData.DamageType m_type;
		public HitData.DamageModifier m_modifier;
	}

	[Serializable]
	public struct DamageModifiers
	{
		public HitData.DamageModifier m_blunt;
		public HitData.DamageModifier m_slash;
		public HitData.DamageModifier m_pierce;
		public HitData.DamageModifier m_chop;
		public HitData.DamageModifier m_pickaxe;
		public HitData.DamageModifier m_fire;
		public HitData.DamageModifier m_frost;
		public HitData.DamageModifier m_lightning;
		public HitData.DamageModifier m_poison;
		public HitData.DamageModifier m_spirit;
	}

	public enum DamageType
	{
		Blunt = 1,
		Slash = 2,
		Pierce = 4,
		Chop = 8,
		Pickaxe = 16,
		Fire = 32,
		Frost = 64,
		Lightning = 128,
		Poison = 256,
		Spirit = 512,
		Physical = 31,
		Elemental = 224,
	}

	public enum DamageModifier
	{
		Normal = 0,
		Resistant = 1,
		Weak = 2,
		Immune = 3,
		Ignore = 4,
		VeryResistant = 5,
		VeryWeak = 6,
	}

	public DamageTypes m_damage;
	public int m_toolTier;
	public bool m_dodgeable;
	public bool m_blockable;
	public bool m_ranged;
	public float m_pushForce;
	public float m_backstabBonus;
	public float m_staggerMultiplier;
	public Vector3 m_point;
	public Vector3 m_dir;
	public string m_statusEffect;
	public Skills.SkillType m_skill;
	public Collider m_hitCollider;
}
