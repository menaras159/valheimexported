using UnityEngine;
using System;
using System.Collections.Generic;

public class CookingStation : MonoBehaviour
{
	[Serializable]
	public class ItemConversion
	{
		public ItemDrop m_from;
		public ItemDrop m_to;
		public float m_cookTime;
	}

	[Serializable]
	public class ItemMessage
	{
		public ItemDrop m_item;
		public string m_message;
	}

	public Switch m_addFoodSwitch;
	public Switch m_addFuelSwitch;
	public EffectList m_addEffect;
	public EffectList m_doneEffect;
	public EffectList m_overcookedEffect;
	public EffectList m_pickEffector;
	public string m_addItemTooltip;
	public Transform m_spawnPoint;
	public float m_spawnForce;
	public ItemDrop m_overCookedItem;
	public List<CookingStation.ItemConversion> m_conversion;
	public List<CookingStation.ItemMessage> m_incompatibleItems;
	public Transform[] m_slots;
	public ParticleSystem[] m_donePS;
	public ParticleSystem[] m_burntPS;
	public string m_name;
	public bool m_requireFire;
	public Transform[] m_fireCheckPoints;
	public float m_fireCheckRadius;
	public bool m_useFuel;
	public ItemDrop m_fuelItem;
	public int m_maxFuel;
	public int m_secPerFuel;
	public EffectList m_fuelAddedEffects;
	public GameObject m_haveFuelObject;
	public GameObject m_haveFireObject;
}
