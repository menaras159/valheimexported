using UnityEngine;
using UnityEngine.UI;

public class ConnectPanel : MonoBehaviour
{
	public Transform m_root;
	public Text m_serverField;
	public Text m_worldField;
	public Text m_statusField;
	public Text m_connections;
	public RectTransform m_playerList;
	public Scrollbar m_playerListScroll;
	public GameObject m_playerElement;
	public InputField m_hostName;
	public InputField m_hostPort;
	public Button m_connectButton;
	public Text m_myPort;
	public Text m_myUID;
	public Text m_knownHosts;
	public Text m_nrOfConnections;
	public Text m_pendingConnections;
	public Toggle m_autoConnect;
	public Text m_zdos;
	public Text m_zdosPool;
	public Text m_zdosSent;
	public Text m_zdosRecv;
	public Text m_zdosInstances;
	public Text m_activePeers;
	public Text m_ntp;
	public Text m_upnp;
	public Text m_dataSent;
	public Text m_dataRecv;
	public Text m_clientSendQueue;
	public Text m_fps;
	public Text m_frameTime;
	public Text m_ping;
	public Text m_quality;
}
