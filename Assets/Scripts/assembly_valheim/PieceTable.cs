using UnityEngine;
using System.Collections.Generic;

public class PieceTable : MonoBehaviour
{
	public List<GameObject> m_pieces;
	public bool m_useCategories;
	public bool m_canRemovePieces;
}
