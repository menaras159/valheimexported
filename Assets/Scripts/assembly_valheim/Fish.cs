using UnityEngine;

public class Fish : MonoBehaviour
{
	public string m_name;
	public float m_swimRange;
	public float m_minDepth;
	public float m_maxDepth;
	public float m_speed;
	public float m_acceleration;
	public float m_turnRate;
	public float m_wpDurationMin;
	public float m_wpDurationMax;
	public float m_avoidSpeedScale;
	public float m_avoidRange;
	public float m_height;
	public float m_eatDuration;
	public float m_hookForce;
	public float m_staminaUse;
	public float m_baseHookChance;
	public GameObject m_pickupItem;
	public int m_pickupItemStackSize;
}
