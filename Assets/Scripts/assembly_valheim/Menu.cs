using UnityEngine;

public class Menu : MonoBehaviour
{
	public Transform m_root;
	public Transform m_menuDialog;
	public Transform m_quitDialog;
	public Transform m_logoutDialog;
	public GameObject m_settingsPrefab;
	public GameObject m_feedbackPrefab;
}
