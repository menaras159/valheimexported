using UnityEngine;

public class Procreation : MonoBehaviour
{
	public float m_updateInterval;
	public float m_totalCheckRange;
	public int m_maxCreatures;
	public float m_partnerCheckRange;
	public float m_pregnancyChance;
	public float m_pregnancyDuration;
	public int m_requiredLovePoints;
	public GameObject m_offspring;
	public int m_minOffspringLevel;
	public float m_spawnOffset;
	public EffectList m_birthEffects;
	public EffectList m_loveEffects;
}
