using UnityEngine;

public class Valkyrie : MonoBehaviour
{
	public float m_startPause;
	public float m_speed;
	public float m_turnRate;
	public float m_dropHeight;
	public float m_startAltitude;
	public float m_descentAltitude;
	public float m_startDistance;
	public float m_startDescentDistance;
	public Vector3 m_attachOffset;
	public float m_textDuration;
	public string m_introTopic;
	[TextAreaAttribute]
	public string m_introText;
	public Transform m_attachPoint;
}
