using UnityEngine;

public class Room : MonoBehaviour
{
	public enum Theme
	{
		Crypt = 1,
		SunkenCrypt = 2,
		Cave = 4,
		ForestCrypt = 8,
		GoblinCamp = 16,
		MeadowsVillage = 32,
		MeadowsFarm = 64,
	}

	public Vector3Int m_size;
	public Theme m_theme;
	public bool m_enabled;
	public bool m_entrance;
	public bool m_endCap;
	public int m_endCapPrio;
	public int m_minPlaceOrder;
	public float m_weight;
	public bool m_faceCenter;
	public bool m_perimeter;
}
