using UnityEngine;

public class CraftingStation : MonoBehaviour
{
	public string m_name;
	public Sprite m_icon;
	public float m_discoverRange;
	public float m_rangeBuild;
	public bool m_craftRequireRoof;
	public bool m_craftRequireFire;
	public Transform m_roofCheckPoint;
	public Transform m_connectionPoint;
	public bool m_showBasicRecipies;
	public float m_useDistance;
	public int m_useAnimation;
	public GameObject m_areaMarker;
	public GameObject m_inUseObject;
	public GameObject m_haveFireObject;
	public EffectList m_craftItemEffects;
	public EffectList m_craftItemDoneEffects;
	public EffectList m_repairItemDoneEffects;
}
