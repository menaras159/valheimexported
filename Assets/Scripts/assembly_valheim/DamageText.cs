using UnityEngine;

public class DamageText : MonoBehaviour
{
	public float m_textDuration;
	public float m_maxTextDistance;
	public int m_largeFontSize;
	public int m_smallFontSize;
	public float m_smallFontDistance;
	public GameObject m_worldTextBase;
}
