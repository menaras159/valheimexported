using UnityEngine;
using System;
using System.Collections.Generic;

public class Trader : MonoBehaviour
{
	[Serializable]
	public class TradeItem
	{
		public ItemDrop m_prefab;
		public int m_stack;
		public int m_price;
	}

	public string m_name;
	public float m_standRange;
	public float m_greetRange;
	public float m_byeRange;
	public List<Trader.TradeItem> m_items;
	public float m_hideDialogDelay;
	public float m_randomTalkInterval;
	public List<string> m_randomTalk;
	public List<string> m_randomGreets;
	public List<string> m_randomGoodbye;
	public List<string> m_randomStartTrade;
	public List<string> m_randomBuy;
	public List<string> m_randomSell;
	public EffectList m_randomTalkFX;
	public EffectList m_randomGreetFX;
	public EffectList m_randomGoodbyeFX;
	public EffectList m_randomStartTradeFX;
	public EffectList m_randomBuyFX;
	public EffectList m_randomSellFX;
}
