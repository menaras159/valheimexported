using UnityEngine;

public class BaseAI : MonoBehaviour
{
	public float m_viewRange;
	public float m_viewAngle;
	public float m_hearRange;
	public EffectList m_alertedEffects;
	public EffectList m_idleSound;
	public float m_idleSoundInterval;
	public float m_idleSoundChance;
	public Pathfinding.AgentType m_pathAgentType;
	public float m_moveMinAngle;
	public bool m_smoothMovement;
	public bool m_serpentMovement;
	public float m_serpentTurnRadius;
	public float m_jumpInterval;
	public float m_randomCircleInterval;
	public float m_randomMoveInterval;
	public float m_randomMoveRange;
	public bool m_randomFly;
	public float m_chanceToTakeoff;
	public float m_chanceToLand;
	public float m_groundDuration;
	public float m_airDuration;
	public float m_maxLandAltitude;
	public float m_flyAltitudeMin;
	public float m_flyAltitudeMax;
	public float m_takeoffTime;
	public bool m_avoidFire;
	public bool m_afraidOfFire;
	public bool m_avoidWater;
	public string m_spawnMessage;
	public string m_deathMessage;
}
