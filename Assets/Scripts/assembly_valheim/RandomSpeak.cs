using UnityEngine;

public class RandomSpeak : MonoBehaviour
{
	public float m_interval;
	public float m_chance;
	public float m_triggerDistance;
	public float m_cullDistance;
	public float m_ttl;
	public Vector3 m_offset;
	public EffectList m_speakEffects;
	public bool m_useLargeDialog;
	public bool m_onlyOnce;
	public string m_topic;
	public string[] m_texts;
}
