using UnityEngine;

public class MineRock5 : MonoBehaviour
{
	public string m_name;
	public float m_health;
	public HitData.DamageModifiers m_damageModifiers;
	public int m_minToolTier;
	public bool m_supportCheck;
	public EffectList m_destroyedEffect;
	public EffectList m_hitEffect;
	public DropTable m_dropItems;
}
