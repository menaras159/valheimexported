using UnityEngine;
using System;
using System.Collections.Generic;

public class SpawnSystem : MonoBehaviour
{
	[Serializable]
	public class SpawnData
	{
		public string m_name;
		public bool m_enabled;
		public GameObject m_prefab;
		public Heightmap.Biome m_biome;
		public Heightmap.BiomeArea m_biomeArea;
		public int m_maxSpawned;
		public float m_spawnInterval;
		public float m_spawnChance;
		public float m_spawnDistance;
		public float m_spawnRadiusMin;
		public float m_spawnRadiusMax;
		public string m_requiredGlobalKey;
		public List<string> m_requiredEnvironments;
		public int m_groupSizeMin;
		public int m_groupSizeMax;
		public float m_groupRadius;
		public bool m_spawnAtNight;
		public bool m_spawnAtDay;
		public float m_minAltitude;
		public float m_maxAltitude;
		public float m_minTilt;
		public float m_maxTilt;
		public bool m_inForest;
		public bool m_outsideForest;
		public float m_minOceanDepth;
		public float m_maxOceanDepth;
		public bool m_huntPlayer;
		public float m_groundOffset;
		public int m_maxLevel;
		public int m_minLevel;
		public float m_levelUpMinCenterDistance;
		public bool m_foldout;
	}

	public List<SpawnSystem.SpawnData> m_spawners;
	public List<Heightmap.Biome> m_biomeFolded;
}
