using UnityEngine;
using System;

public class PickableItem : MonoBehaviour
{
	[Serializable]
	public struct RandomItem
	{
		public ItemDrop m_itemPrefab;
		public int m_stackMin;
		public int m_stackMax;
	}

	public ItemDrop m_itemPrefab;
	public int m_stack;
	public RandomItem[] m_randomItemPrefabs;
	public EffectList m_pickEffector;
}
