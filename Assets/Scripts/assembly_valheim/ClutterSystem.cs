using UnityEngine;
using System;
using System.Collections.Generic;

public class ClutterSystem : MonoBehaviour
{
	[Serializable]
	public class Clutter
	{
		public string m_name;
		public bool m_enabled;
		public Heightmap.Biome m_biome;
		public bool m_instanced;
		public GameObject m_prefab;
		public int m_amount;
		public bool m_onUncleared;
		public bool m_onCleared;
		public float m_scaleMin;
		public float m_scaleMax;
		public float m_maxTilt;
		public float m_maxAlt;
		public float m_minAlt;
		public bool m_snapToWater;
		public bool m_terrainTilt;
		public float m_randomOffset;
		public float m_minOceanDepth;
		public float m_maxOceanDepth;
		public bool m_inForest;
		public float m_forestTresholdMin;
		public float m_forestTresholdMax;
		public float m_fractalScale;
		public float m_fractalOffset;
		public float m_fractalTresholdMin;
		public float m_fractalTresholdMax;
	}

	public List<ClutterSystem.Clutter> m_clutter;
	public float m_grassPatchSize;
	public float m_distance;
	public float m_waterLevel;
	public float m_playerPushFade;
	public float m_amountScale;
	public bool m_menuHack;
}
