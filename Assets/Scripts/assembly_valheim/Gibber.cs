using UnityEngine;
using System;

public class Gibber : MonoBehaviour
{
	[Serializable]
	public class GibbData
	{
		public GameObject m_object;
		public float m_chanceToSpawn;
	}

	public EffectList m_punchEffector;
	public GameObject m_gibHitEffect;
	public GameObject m_gibDestroyEffect;
	public float m_gibHitDestroyChance;
	public GibbData[] m_gibbs;
	public float m_minVel;
	public float m_maxVel;
	public float m_maxRotVel;
	public float m_impactDirectionMix;
	public float m_timeout;
}
