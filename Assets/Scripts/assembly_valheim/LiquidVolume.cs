using UnityEngine;

public class LiquidVolume : MonoBehaviour
{
	public int m_width;
	public float m_scale;
	public float m_maxDepth;
	public LiquidType m_liquidType;
	public float m_physicsOffset;
	public float m_initialVolume;
	public int m_initialArea;
	public float m_viscocity;
	public float m_noiseHeight;
	public float m_noiseFrequency;
	public float m_noiseSpeed;
	public Material m_material;
	public bool m_castShadow;
	public LayerMask m_groundLayer;
	public MeshCollider m_collider;
	public float m_saveInterval;
	public float m_randomEffectInterval;
	public EffectList m_randomEffectList;
}
