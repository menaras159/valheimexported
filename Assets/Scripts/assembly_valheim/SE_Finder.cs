public class SE_Finder : StatusEffect
{
	public EffectList m_pingEffectNear;
	public EffectList m_pingEffectMed;
	public EffectList m_pingEffectFar;
	public float m_closerTriggerDistance;
	public float m_furtherTriggerDistance;
	public float m_closeFrequency;
	public float m_distantFrequency;
}
