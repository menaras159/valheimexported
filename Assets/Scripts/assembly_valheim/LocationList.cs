using UnityEngine;
using System.Collections.Generic;

public class LocationList : MonoBehaviour
{
	public int m_sortOrder;
	public List<ZoneSystem.ZoneLocation> m_locations;
}
