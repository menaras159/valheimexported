using UnityEngine;

public class CamShaker : MonoBehaviour
{
	public float m_strength;
	public float m_range;
	public float m_delay;
	public bool m_continous;
	public float m_continousDuration;
	public bool m_localOnly;
}
