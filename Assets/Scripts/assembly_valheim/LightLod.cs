using UnityEngine;

public class LightLod : MonoBehaviour
{
	public bool m_lightLod;
	public float m_lightDistance;
	public bool m_shadowLod;
	public float m_shadowDistance;
}
