using UnityEngine;

public class Leviathan : MonoBehaviour
{
	public float m_waveScale;
	public float m_floatOffset;
	public float m_movementSpeed;
	public float m_maxSpeed;
	public MineRock m_mineRock;
	public float m_hitReactionChance;
	public int m_leaveDelay;
	public EffectList m_reactionEffects;
	public EffectList m_leaveEffects;
}
