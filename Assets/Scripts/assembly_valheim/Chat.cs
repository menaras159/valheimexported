using UnityEngine;

public class Chat : Terminal
{
	public float m_hideDelay;
	public float m_worldTextTTL;
	public GameObject m_worldTextBase;
	public GameObject m_npcTextBase;
	public GameObject m_npcTextBaseLarge;
}
