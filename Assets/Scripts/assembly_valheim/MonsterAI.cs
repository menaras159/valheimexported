using System.Collections.Generic;

public class MonsterAI : BaseAI
{
	public float m_alertRange;
	public bool m_fleeIfHurtWhenTargetCantBeReached;
	public bool m_fleeIfNotAlerted;
	public float m_fleeIfLowHealth;
	public bool m_circulateWhileCharging;
	public bool m_circulateWhileChargingFlying;
	public bool m_enableHuntPlayer;
	public bool m_attackPlayerObjects;
	public float m_interceptTimeMax;
	public float m_interceptTimeMin;
	public float m_maxChaseDistance;
	public float m_minAttackInterval;
	public float m_circleTargetInterval;
	public float m_circleTargetDuration;
	public float m_circleTargetDistance;
	public bool m_sleeping;
	public bool m_noiseWakeup;
	public float m_noiseRangeScale;
	public float m_wakeupRange;
	public EffectList m_wakeupEffects;
	public bool m_avoidLand;
	public List<ItemDrop> m_consumeItems;
	public float m_consumeRange;
	public float m_consumeSearchRange;
	public float m_consumeSearchInterval;
}
