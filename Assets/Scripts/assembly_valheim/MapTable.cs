using UnityEngine;

public class MapTable : MonoBehaviour
{
	public string m_name;
	public Switch m_readSwitch;
	public Switch m_writeSwitch;
	public EffectList m_writeEffects;
}
