using UnityEngine;

public class Ragdoll : MonoBehaviour
{
	public float m_velMultiplier;
	public float m_ttl;
	public Renderer m_mainModel;
	public EffectList m_removeEffect;
	public bool m_float;
	public float m_floatOffset;
}
