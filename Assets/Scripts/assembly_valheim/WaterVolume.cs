using UnityEngine;

public class WaterVolume : MonoBehaviour
{
	public MeshRenderer m_waterSurface;
	public Heightmap m_heightmap;
	public float m_forceDepth;
	public float m_surfaceOffset;
	public bool m_menuWater;
	public bool m_useGlobalWind;
}
