using System;
using UnityEngine;

[Serializable]
public class EffectList
{
	[Serializable]
	public class EffectData
	{
		public GameObject m_prefab;
		public bool m_enabled;
		public int m_variant;
		public bool m_attach;
		public bool m_inheritParentRotation;
		public bool m_inheritParentScale;
		public bool m_randomRotation;
		public bool m_scale;
		public string m_childTransform;
	}

	public EffectData[] m_effectPrefabs;
}
