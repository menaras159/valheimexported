using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Audio;

public class AudioMan : MonoBehaviour
{
	[Serializable]
	public class BiomeAmbients
	{
		public string m_name;
		public Heightmap.Biome m_biome;
		public List<AudioClip> m_randomAmbientClips;
		public List<AudioClip> m_randomAmbientClipsDay;
		public List<AudioClip> m_randomAmbientClipsNight;
	}

	public AudioMixerGroup m_ambientMixer;
	public AudioMixer m_masterMixer;
	public float m_snapshotTransitionTime;
	public AudioClip m_windAudio;
	public float m_windMinVol;
	public float m_windMaxVol;
	public float m_windMinPitch;
	public float m_windMaxPitch;
	public float m_windVariation;
	public float m_windIntensityPower;
	public AudioClip m_oceanAudio;
	public float m_oceanVolumeMax;
	public float m_oceanVolumeMin;
	public float m_oceanFadeSpeed;
	public float m_oceanMoveSpeed;
	public float m_oceanDepthTreshold;
	public float m_ambientFadeTime;
	public float m_randomAmbientInterval;
	public float m_randomAmbientChance;
	public float m_randomMinPitch;
	public float m_randomMaxPitch;
	public float m_randomMinVol;
	public float m_randomMaxVol;
	public float m_randomPan;
	public float m_randomFadeIn;
	public float m_randomFadeOut;
	public float m_randomMinDistance;
	public float m_randomMaxDistance;
	public List<AudioMan.BiomeAmbients> m_randomAmbients;
	public GameObject m_randomAmbientPrefab;
}
