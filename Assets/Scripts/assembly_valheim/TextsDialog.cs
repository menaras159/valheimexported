using UnityEngine;
using UnityEngine.UI;

public class TextsDialog : MonoBehaviour
{
	public RectTransform m_listRoot;
	public GameObject m_elementPrefab;
	public Text m_totalSkillText;
	public float m_spacing;
	public Text m_textAreaTopic;
	public Text m_textArea;
	public ScrollRectEnsureVisible m_recipeEnsureVisible;
}
