using UnityEngine;

public class CircleProjector : MonoBehaviour
{
	public float m_radius;
	public int m_nrOfSegments;
	public GameObject m_prefab;
	public LayerMask m_mask;
}
