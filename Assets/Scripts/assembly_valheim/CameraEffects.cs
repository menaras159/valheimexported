using UnityEngine;

public class CameraEffects : MonoBehaviour
{
	public bool m_forceDof;
	public LayerMask m_dofRayMask;
	public bool m_dofAutoFocus;
	public float m_dofMinDistance;
	public float m_dofMinDistanceShip;
	public float m_dofMaxDistance;
}
