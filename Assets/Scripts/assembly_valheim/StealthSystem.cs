using UnityEngine;

public class StealthSystem : MonoBehaviour
{
	public LayerMask m_shadowTestMask;
	public float m_minLightLevel;
	public float m_maxLightLevel;
}
