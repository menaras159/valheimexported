using UnityEngine;

public class PrivateArea : MonoBehaviour
{
	public string m_name;
	public float m_radius;
	public float m_updateConnectionsInterval;
	public GameObject m_enabledEffect;
	public CircleProjector m_areaMarker;
	public EffectList m_flashEffect;
	public EffectList m_activateEffect;
	public EffectList m_deactivateEffect;
	public EffectList m_addPermittedEffect;
	public EffectList m_removedPermittedEffect;
	public GameObject m_connectEffect;
	public GameObject m_inRangeEffect;
	public MeshRenderer m_model;
}
