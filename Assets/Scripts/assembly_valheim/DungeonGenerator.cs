using UnityEngine;
using System;
using System.Collections.Generic;

public class DungeonGenerator : MonoBehaviour
{
	[Serializable]
	public class DoorDef
	{
		public GameObject m_prefab;
		public string m_connectionType;
	}

	public enum Algorithm
	{
		Dungeon = 0,
		CampGrid = 1,
		CampRadial = 2,
	}

	public Algorithm m_algorithm;
	public int m_maxRooms;
	public int m_minRooms;
	public int m_minRequiredRooms;
	public List<string> m_requiredRooms;
	public Room.Theme m_themes;
	public List<DungeonGenerator.DoorDef> m_doorTypes;
	public float m_doorChance;
	public float m_maxTilt;
	public float m_tileWidth;
	public int m_gridSize;
	public float m_spawnChance;
	public float m_campRadiusMin;
	public float m_campRadiusMax;
	public float m_minAltitude;
	public int m_perimeterSections;
	public float m_perimeterBuffer;
	public Vector3 m_zoneCenter;
	public Vector3 m_zoneSize;
}
