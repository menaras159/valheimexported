using UnityEngine;

public class DepthCamera : MonoBehaviour
{
	public Shader m_depthShader;
	public float m_offset;
	public RenderTexture m_texture;
	public float m_updateInterval;
}
