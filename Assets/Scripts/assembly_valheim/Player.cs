using UnityEngine;

public class Player : Humanoid
{
	public float m_maxPlaceDistance;
	public float m_maxInteractDistance;
	public float m_scrollSens;
	public float m_staminaRegen;
	public float m_staminaRegenTimeMultiplier;
	public float m_staminaRegenDelay;
	public float m_runStaminaDrain;
	public float m_sneakStaminaDrain;
	public float m_swimStaminaDrainMinSkill;
	public float m_swimStaminaDrainMaxSkill;
	public float m_dodgeStaminaUsage;
	public float m_weightStaminaFactor;
	public float m_autoPickupRange;
	public float m_maxCarryWeight;
	public float m_encumberedStaminaDrain;
	public float m_hardDeathCooldown;
	public float m_baseCameraShake;
	public float m_placeDelay;
	public float m_removeDelay;
	public EffectList m_drownEffects;
	public EffectList m_spawnEffects;
	public EffectList m_removeEffects;
	public EffectList m_dodgeEffects;
	public EffectList m_autopickupEffects;
	public EffectList m_skillLevelupEffects;
	public EffectList m_equipStartEffects;
	public GameObject m_placeMarker;
	public GameObject m_tombstone;
	public GameObject m_valkyrie;
	public Sprite m_textIcon;
	public float m_baseHP;
	public float m_baseStamina;
	public float m_guardianPowerCooldown;
}
