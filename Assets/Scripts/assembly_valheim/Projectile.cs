using UnityEngine;

public class Projectile : MonoBehaviour
{
	public HitData.DamageTypes m_damage;
	public float m_aoe;
	public bool m_dodgeable;
	public bool m_blockable;
	public float m_attackForce;
	public float m_backstabBonus;
	public string m_statusEffect;
	public bool m_canHitWater;
	public float m_ttl;
	public float m_gravity;
	public float m_rayRadius;
	public float m_hitNoise;
	public bool m_stayAfterHitStatic;
	public GameObject m_hideOnHit;
	public bool m_stopEmittersOnHit;
	public EffectList m_hitEffects;
	public EffectList m_hitWaterEffects;
	public bool m_respawnItemOnHit;
	public GameObject m_spawnOnHit;
	public float m_spawnOnHitChance;
	public bool m_showBreakMessage;
	public bool m_staticHitOnly;
	public bool m_groundHitOnly;
	public Vector3 m_spawnOffset;
	public bool m_spawnRandomRotation;
	public EffectList m_spawnOnHitEffects;
	public float m_rotateVisual;
	public GameObject m_visual;
}
