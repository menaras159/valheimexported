using UnityEngine;

public class Chair : MonoBehaviour
{
	public string m_name;
	public float m_useDistance;
	public Transform m_attachPoint;
	public Vector3 m_detachOffset;
	public string m_attachAnimation;
	public bool m_inShip;
}
