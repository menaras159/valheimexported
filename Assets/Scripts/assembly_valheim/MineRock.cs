using UnityEngine;

public class MineRock : MonoBehaviour
{
	public string m_name;
	public float m_health;
	public bool m_removeWhenDestroyed;
	public HitData.DamageModifiers m_damageModifiers;
	public int m_minToolTier;
	public GameObject m_areaRoot;
	public GameObject m_baseModel;
	public EffectList m_destroyedEffect;
	public EffectList m_hitEffect;
	public DropTable m_dropItems;
}
