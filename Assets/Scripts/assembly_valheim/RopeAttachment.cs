using UnityEngine;

public class RopeAttachment : MonoBehaviour
{
	public string m_name;
	public string m_hoverText;
	public float m_pullDistance;
	public float m_pullForce;
	public float m_maxPullVel;
}
