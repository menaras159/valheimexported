using UnityEngine;
using System;
using UnityEngine.Audio;
using System.Collections.Generic;

public class MusicMan : MonoBehaviour
{
	[Serializable]
	public class NamedMusic
	{
		public string m_name;
		public AudioClip[] m_clips;
		public float m_volume;
		public float m_fadeInTime;
		public bool m_alwaysFadeout;
		public bool m_loop;
		public bool m_resume;
		public bool m_enabled;
		public bool m_ambientMusic;
	}

	public AudioMixerGroup m_musicMixer;
	public List<MusicMan.NamedMusic> m_music;
	public float m_combatMusicTimeout;
	public float m_sailMusicShipSpeedThreshold;
	public float m_sailMusicMinSailTime;
	public float m_randomMusicIntervalMin;
	public float m_randomMusicIntervalMax;
}
