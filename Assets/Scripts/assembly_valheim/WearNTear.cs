using UnityEngine;

public class WearNTear : MonoBehaviour
{
	public enum MaterialType
	{
		Wood = 0,
		Stone = 1,
		Iron = 2,
		HardWood = 3,
	}

	public GameObject m_new;
	public GameObject m_worn;
	public GameObject m_broken;
	public GameObject m_wet;
	public bool m_noRoofWear;
	public bool m_noSupportWear;
	public MaterialType m_materialType;
	public bool m_supports;
	public Vector3 m_comOffset;
	public float m_health;
	public HitData.DamageModifiers m_damages;
	public float m_hitNoise;
	public float m_destroyNoise;
	public EffectList m_destroyedEffect;
	public EffectList m_hitEffect;
	public EffectList m_switchEffect;
	public bool m_autoCreateFragments;
	public GameObject[] m_fragmentRoots;
}
