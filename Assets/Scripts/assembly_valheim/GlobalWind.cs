using UnityEngine;

public class GlobalWind : MonoBehaviour
{
	public float m_multiplier;
	public bool m_smoothUpdate;
	public bool m_alignToWindDirection;
	public bool m_particleVelocity;
	public bool m_particleForce;
	public bool m_particleEmission;
	public int m_particleEmissionMin;
	public int m_particleEmissionMax;
	public float m_clothRandomAccelerationFactor;
	public bool m_checkPlayerShelter;
}
