using UnityEngine;
using System;
using System.Collections.Generic;

public class Skills : MonoBehaviour
{
	[Serializable]
	public class SkillDef
	{
		public Skills.SkillType m_skill;
		public Sprite m_icon;
		public string m_description;
		public float m_increseStep;
	}

	public enum SkillType
	{
		None = 0,
		Swords = 1,
		Knives = 2,
		Clubs = 3,
		Polearms = 4,
		Spears = 5,
		Blocking = 6,
		Axes = 7,
		Bows = 8,
		FireMagic = 9,
		FrostMagic = 10,
		Unarmed = 11,
		Pickaxes = 12,
		WoodCutting = 13,
		Jump = 100,
		Sneak = 101,
		Run = 102,
		Swim = 103,
		Ride = 110,
		All = 999,
	}

	public bool m_useSkillCap;
	public float m_totalSkillCap;
	public List<Skills.SkillDef> m_skills;
	public float m_DeathLowerFactor;
}
