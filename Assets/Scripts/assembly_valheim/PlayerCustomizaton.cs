using UnityEngine;
using UnityEngine.UI;

public class PlayerCustomizaton : MonoBehaviour
{
	public Color m_skinColor0;
	public Color m_skinColor1;
	public Color m_hairColor0;
	public Color m_hairColor1;
	public float m_hairMaxLevel;
	public float m_hairMinLevel;
	public Text m_selectedBeard;
	public Text m_selectedHair;
	public Slider m_skinHue;
	public Slider m_hairLevel;
	public Slider m_hairTone;
	public RectTransform m_beardPanel;
	public Toggle m_maleToggle;
	public Toggle m_femaleToggle;
	public ItemDrop m_noHair;
	public ItemDrop m_noBeard;
}
