using System;
using UnityEngine;

[Serializable]
public class Attack
{
	public enum AttackType
	{
		Horizontal = 0,
		Vertical = 1,
		Projectile = 2,
		None = 3,
		Area = 4,
		TriggerProjectile = 5,
	}

	public enum HitPointType
	{
		Closest = 0,
		Average = 1,
		First = 2,
	}

	public AttackType m_attackType;
	public string m_attackAnimation;
	public int m_attackRandomAnimations;
	public int m_attackChainLevels;
	public bool m_consumeItem;
	public bool m_hitTerrain;
	public float m_attackStamina;
	public float m_speedFactor;
	public float m_speedFactorRotation;
	public float m_attackStartNoise;
	public float m_attackHitNoise;
	public float m_damageMultiplier;
	public float m_forceMultiplier;
	public float m_staggerMultiplier;
	public string m_attackOriginJoint;
	public float m_attackRange;
	public float m_attackHeight;
	public float m_attackOffset;
	public GameObject m_spawnOnTrigger;
	public float m_attackAngle;
	public float m_attackRayWidth;
	public float m_maxYAngle;
	public bool m_lowerDamagePerHit;
	public HitPointType m_hitPointtype;
	public bool m_hitThroughWalls;
	public bool m_multiHit;
	public bool m_pickaxeSpecial;
	public float m_lastChainDamageMultiplier;
	public DestructibleType m_resetChainIfHit;
	public DestructibleType m_skillHitType;
	public Skills.SkillType m_specialHitSkill;
	public DestructibleType m_specialHitType;
	public GameObject m_attackProjectile;
	public float m_projectileVel;
	public float m_projectileVelMin;
	public float m_projectileAccuracy;
	public float m_projectileAccuracyMin;
	public bool m_useCharacterFacing;
	public bool m_useCharacterFacingYAim;
	public float m_launchAngle;
	public int m_projectiles;
	public int m_projectileBursts;
	public float m_burstInterval;
	public bool m_destroyPreviousProjectile;
	public EffectList m_hitEffect;
	public EffectList m_hitTerrainEffect;
	public EffectList m_startEffect;
	public EffectList m_triggerEffect;
	public EffectList m_trailStartEffect;
}
