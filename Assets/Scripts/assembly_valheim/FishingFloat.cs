using UnityEngine;

public class FishingFloat : MonoBehaviour
{
	public float m_maxDistance;
	public float m_moveForce;
	public float m_pullLineSpeed;
	public float m_pullStaminaUse;
	public float m_hookedStaminaPerSec;
	public float m_breakDistance;
	public float m_range;
	public float m_nibbleForce;
	public EffectList m_nibbleEffect;
	public EffectList m_lineBreakEffect;
	public float m_maxLineSlack;
	public LineConnect m_rodLine;
	public LineConnect m_hookLine;
}
