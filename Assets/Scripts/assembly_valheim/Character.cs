using UnityEngine;

public class Character : MonoBehaviour
{
	public enum Faction
	{
		Players = 0,
		AnimalsVeg = 1,
		ForestMonsters = 2,
		Undead = 3,
		Demon = 4,
		MountainMonsters = 5,
		SeaMonsters = 6,
		PlainsMonsters = 7,
		Boss = 8,
	}

	public enum GroundTiltType
	{
		None = 0,
		Pitch = 1,
		Full = 2,
		PitchRaycast = 3,
		FullRaycast = 4,
	}

	public string m_name;
	public string m_group;
	public Faction m_faction;
	public bool m_boss;
	public string m_bossEvent;
	public string m_defeatSetGlobalKey;
	public float m_crouchSpeed;
	public float m_walkSpeed;
	public float m_speed;
	public float m_turnSpeed;
	public float m_runSpeed;
	public float m_runTurnSpeed;
	public float m_flySlowSpeed;
	public float m_flyFastSpeed;
	public float m_flyTurnSpeed;
	public float m_acceleration;
	public float m_jumpForce;
	public float m_jumpForceForward;
	public float m_jumpForceTiredFactor;
	public float m_airControl;
	public bool m_canSwim;
	public float m_swimDepth;
	public float m_swimSpeed;
	public float m_swimTurnSpeed;
	public float m_swimAcceleration;
	public GroundTiltType m_groundTilt;
	public bool m_flying;
	public float m_jumpStaminaUsage;
	public Transform m_eye;
	public EffectList m_hitEffects;
	public EffectList m_critHitEffects;
	public EffectList m_backstabHitEffects;
	public EffectList m_deathEffects;
	public EffectList m_waterEffects;
	public EffectList m_tarEffects;
	public EffectList m_slideEffects;
	public EffectList m_jumpEffects;
	public bool m_tolerateWater;
	public bool m_tolerateFire;
	public bool m_tolerateSmoke;
	public bool m_tolerateTar;
	public float m_health;
	public HitData.DamageModifiers m_damageModifiers;
	public bool m_staggerWhenBlocked;
	public float m_staggerDamageFactor;
}
