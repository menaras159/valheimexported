using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Audio;

public class Settings : MonoBehaviour
{
	[Serializable]
	public class KeySetting
	{
		public string m_keyName;
		public RectTransform m_keyTransform;
	}

	public Slider m_sensitivitySlider;
	public Slider m_gamepadSensitivitySlider;
	public Toggle m_invertMouse;
	public Toggle m_gamepadEnabled;
	public GameObject m_bindDialog;
	public List<Settings.KeySetting> m_keys;
	public Toggle m_cameraShake;
	public Toggle m_shipCameraTilt;
	public Toggle m_quickPieceSelect;
	public Toggle m_showKeyHints;
	public Slider m_guiScaleSlider;
	public Text m_guiScaleText;
	public Text m_language;
	public Button m_resetTutorial;
	public Slider m_volumeSlider;
	public Slider m_sfxVolumeSlider;
	public Slider m_musicVolumeSlider;
	public Toggle m_continousMusic;
	public AudioMixer m_masterMixer;
	public Toggle m_dofToggle;
	public Toggle m_vsyncToggle;
	public Toggle m_bloomToggle;
	public Toggle m_ssaoToggle;
	public Toggle m_sunshaftsToggle;
	public Toggle m_aaToggle;
	public Toggle m_caToggle;
	public Toggle m_motionblurToggle;
	public Toggle m_tesselationToggle;
	public Toggle m_softPartToggle;
	public Toggle m_fullscreenToggle;
	public Slider m_shadowQuality;
	public Text m_shadowQualityText;
	public Slider m_lod;
	public Text m_lodText;
	public Slider m_lights;
	public Text m_lightsText;
	public Slider m_vegetation;
	public Text m_vegetationText;
	public Slider m_pointLights;
	public Text m_pointLightsText;
	public Slider m_pointLightShadows;
	public Text m_pointLightShadowsText;
	public Text m_resButtonText;
	public GameObject m_resDialog;
	public GameObject m_resListElement;
	public RectTransform m_resListRoot;
	public Scrollbar m_resListScroll;
	public float m_resListSpace;
	public GameObject m_resSwitchDialog;
	public Text m_resSwitchCountdown;
	public int m_minResWidth;
	public int m_minResHeight;
}
