using System;
using UnityEngine;

[Serializable]
public class EnvSetup
{
	public string m_name;
	public bool m_default;
	public bool m_isWet;
	public bool m_isFreezing;
	public bool m_isFreezingAtNight;
	public bool m_isCold;
	public bool m_isColdAtNight;
	public bool m_alwaysDark;
	public Color m_ambColorNight;
	public Color m_ambColorDay;
	public Color m_fogColorNight;
	public Color m_fogColorMorning;
	public Color m_fogColorDay;
	public Color m_fogColorEvening;
	public Color m_fogColorSunNight;
	public Color m_fogColorSunMorning;
	public Color m_fogColorSunDay;
	public Color m_fogColorSunEvening;
	public float m_fogDensityNight;
	public float m_fogDensityMorning;
	public float m_fogDensityDay;
	public float m_fogDensityEvening;
	public Color m_sunColorNight;
	public Color m_sunColorMorning;
	public Color m_sunColorDay;
	public Color m_sunColorEvening;
	public float m_lightIntensityDay;
	public float m_lightIntensityNight;
	public float m_sunAngle;
	public float m_windMin;
	public float m_windMax;
	public GameObject m_envObject;
	public GameObject[] m_psystems;
	public bool m_psystemsOutsideOnly;
	public float m_rainCloudAlpha;
	public AudioClip m_ambientLoop;
	public float m_ambientVol;
	public string m_ambientList;
	public string m_musicMorning;
	public string m_musicEvening;
	public string m_musicDay;
	public string m_musicNight;
}
