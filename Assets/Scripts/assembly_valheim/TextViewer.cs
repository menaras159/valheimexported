using UnityEngine;
using UnityEngine.UI;

public class TextViewer : MonoBehaviour
{
	public GameObject m_root;
	public Text m_topic;
	public Text m_text;
	public Text m_runeText;
	public GameObject m_closeText;
	public GameObject m_introRoot;
	public Text m_introTopic;
	public Text m_introText;
	public GameObject m_ravenRoot;
	public Text m_ravenTopic;
	public Text m_ravenText;
}
