using UnityEngine;

public class MeleeWeaponTrail : MonoBehaviour
{
	[SerializeField]
	private bool _emit;
	[SerializeField]
	private float _emitTime;
	[SerializeField]
	private Material _material;
	[SerializeField]
	private float _lifeTime;
	[SerializeField]
	private Color[] _colors;
	[SerializeField]
	private float[] _sizes;
	[SerializeField]
	private float _minVertexDistance;
	[SerializeField]
	private float _maxVertexDistance;
	[SerializeField]
	private float _maxAngle;
	[SerializeField]
	private bool _autoDestruct;
	[SerializeField]
	private int subdivisions;
	[SerializeField]
	private Transform _base;
	[SerializeField]
	private Transform _tip;
}
