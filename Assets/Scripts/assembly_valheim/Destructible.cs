using UnityEngine;

public class Destructible : MonoBehaviour
{
	public DestructibleType m_destructibleType;
	public float m_health;
	public HitData.DamageModifiers m_damages;
	public float m_minDamageTreshold;
	public int m_minToolTier;
	public float m_hitNoise;
	public float m_destroyNoise;
	public float m_ttl;
	public GameObject m_spawnWhenDestroyed;
	public EffectList m_destroyedEffect;
	public EffectList m_hitEffect;
	public bool m_autoCreateFragments;
}
