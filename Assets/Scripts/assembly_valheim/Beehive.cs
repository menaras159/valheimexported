using UnityEngine;

public class Beehive : MonoBehaviour
{
	public string m_name;
	public Transform m_coverPoint;
	public Transform m_spawnPoint;
	public GameObject m_beeEffect;
	public float m_maxCover;
	public Heightmap.Biome m_biome;
	public float m_secPerUnit;
	public int m_maxHoney;
	public ItemDrop m_honeyItem;
	public EffectList m_spawnEffect;
}
