using UnityEngine;

public class Smoke : MonoBehaviour
{
	public Vector3 m_vel;
	public float m_randomVel;
	public float m_force;
	public float m_ttl;
	public float m_fadetime;
}
