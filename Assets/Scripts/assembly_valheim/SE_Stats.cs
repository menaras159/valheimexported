using System.Collections.Generic;

public class SE_Stats : StatusEffect
{
	public float m_tickInterval;
	public float m_healthPerTickMinHealthPercentage;
	public float m_healthPerTick;
	public float m_healthOverTime;
	public float m_healthOverTimeDuration;
	public float m_healthOverTimeInterval;
	public float m_staminaOverTime;
	public float m_staminaOverTimeDuration;
	public float m_staminaDrainPerSec;
	public float m_runStaminaDrainModifier;
	public float m_jumpStaminaUseModifier;
	public float m_healthRegenMultiplier;
	public float m_staminaRegenMultiplier;
	public Skills.SkillType m_raiseSkill;
	public float m_raiseSkillModifier;
	public List<HitData.DamageModPair> m_mods;
	public Skills.SkillType m_modifyAttackSkill;
	public float m_damageModifier;
	public float m_noiseModifier;
	public float m_stealthModifier;
	public float m_addMaxCarryWeight;
	public float m_speedModifier;
}
