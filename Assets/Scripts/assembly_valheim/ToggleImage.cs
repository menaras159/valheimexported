using UnityEngine;
using UnityEngine.UI;

public class ToggleImage : MonoBehaviour
{
	public Image m_targetImage;
	public Sprite m_onImage;
	public Sprite m_offImage;
}
