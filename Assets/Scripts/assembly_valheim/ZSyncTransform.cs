using UnityEngine;

public class ZSyncTransform : MonoBehaviour
{
	public bool m_syncPosition;
	public bool m_syncRotation;
	public bool m_syncScale;
	public bool m_syncBodyVelocity;
	public bool m_characterParentSync;
}
