using UnityEngine;

public class Door : MonoBehaviour
{
	public string m_name;
	public GameObject m_doorObject;
	public ItemDrop m_keyItem;
	public EffectList m_openEffects;
	public EffectList m_closeEffects;
	public EffectList m_lockedEffects;
}
