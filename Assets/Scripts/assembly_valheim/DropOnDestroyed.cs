using UnityEngine;

public class DropOnDestroyed : MonoBehaviour
{
	public DropTable m_dropWhenDestroyed;
	public float m_spawnYOffset;
	public float m_spawnYStep;
}
