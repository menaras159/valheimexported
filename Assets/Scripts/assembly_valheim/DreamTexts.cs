using UnityEngine;
using System;
using System.Collections.Generic;

public class DreamTexts : MonoBehaviour
{
	[Serializable]
	public class DreamText
	{
		public string m_text;
		public float m_chanceToDream;
		public List<string> m_trueKeys;
		public List<string> m_falseKeys;
	}

	public List<DreamTexts.DreamText> m_texts;
}
