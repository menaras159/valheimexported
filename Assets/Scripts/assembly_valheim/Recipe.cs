using UnityEngine;

public class Recipe : ScriptableObject
{
	public ItemDrop m_item;
	public int m_amount;
	public bool m_enabled;
	public CraftingStation m_craftingStation;
	public CraftingStation m_repairStation;
	public int m_minStationLevel;
	public Piece.Requirement[] m_resources;
}
