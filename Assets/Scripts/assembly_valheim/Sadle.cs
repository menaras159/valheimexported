using UnityEngine;

public class Sadle : MonoBehaviour
{
	public string m_hoverText;
	public float m_maxUseRange;
	public Transform m_attachPoint;
	public Vector3 m_detachOffset;
	public string m_attachAnimation;
	public float m_maxStamina;
	public float m_runStaminaDrain;
	public float m_swimStaminaDrain;
	public float m_staminaRegen;
	public float m_staminaRegenHungry;
	public EffectList m_drownEffects;
}
