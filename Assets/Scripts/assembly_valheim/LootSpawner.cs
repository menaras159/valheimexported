using UnityEngine;

public class LootSpawner : MonoBehaviour
{
	public DropTable m_items;
	public EffectList m_spawnEffect;
	public float m_respawnTimeMinuts;
	public bool m_spawnAtNight;
	public bool m_spawnAtDay;
	public bool m_spawnWhenEnemiesCleared;
	public float m_enemiesCheckRange;
}
