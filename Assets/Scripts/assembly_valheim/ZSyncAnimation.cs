using UnityEngine;
using System.Collections.Generic;

public class ZSyncAnimation : MonoBehaviour
{
	public List<string> m_syncBools;
	public List<string> m_syncFloats;
	public List<string> m_syncInts;
	public bool m_smoothCharacterSpeeds;
}
