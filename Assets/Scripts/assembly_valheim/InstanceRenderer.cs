using UnityEngine;
using UnityEngine.Rendering;

public class InstanceRenderer : MonoBehaviour
{
	public Mesh m_mesh;
	public Material m_material;
	public Vector3 m_scale;
	public bool m_frustumCull;
	public bool m_useLod;
	public bool m_useXZLodDistance;
	public float m_lodMinDistance;
	public float m_lodMaxDistance;
	public ShadowCastingMode m_shadowCasting;
}
