using UnityEngine;

public class MenuScene : MonoBehaviour
{
	public Light m_dirLight;
	public Color m_sunFogColor;
	public Color m_fogColor;
	public Color m_ambientLightColor;
	public float m_fogDensity;
	public Vector3 m_windDir;
	public float m_windIntensity;
}
