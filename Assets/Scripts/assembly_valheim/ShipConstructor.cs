using UnityEngine;

public class ShipConstructor : MonoBehaviour
{
	public GameObject m_shipPrefab;
	public GameObject m_hideWhenConstructed;
	public Transform m_spawnPoint;
	public long m_constructionTimeMinutes;
}
