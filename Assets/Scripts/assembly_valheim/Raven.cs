using UnityEngine;
using System;
using System.Collections.Generic;

public class Raven : MonoBehaviour
{
	[Serializable]
	public class RavenText
	{
		public bool m_alwaysSpawn;
		public bool m_munin;
		public int m_priority;
		public string m_key;
		public string m_topic;
		public string m_label;
		[TextAreaAttribute]
		public string m_text;
	}

	public GameObject m_visual;
	public GameObject m_exclamation;
	public string m_name;
	public bool m_isMunin;
	public bool m_autoTalk;
	public float m_idleEffectIntervalMin;
	public float m_idleEffectIntervalMax;
	public float m_spawnDistance;
	public float m_despawnDistance;
	public float m_autoTalkDistance;
	public float m_enemyCheckDistance;
	public float m_rotateSpeed;
	public float m_minRotationAngle;
	public float m_dialogVisibleTime;
	public float m_longDialogVisibleTime;
	public float m_dontFlyDistance;
	public float m_textOffset;
	public float m_textCullDistance;
	public float m_randomTextInterval;
	public float m_randomTextIntervalImportant;
	public List<string> m_randomTextsImportant;
	public List<string> m_randomTexts;
	public EffectList m_idleEffect;
	public EffectList m_despawnEffect;
}
