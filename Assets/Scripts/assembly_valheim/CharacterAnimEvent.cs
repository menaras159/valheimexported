using UnityEngine;
using System;

public class CharacterAnimEvent : MonoBehaviour
{
	[Serializable]
	public class Foot
	{
		public Foot(Transform t, AvatarIKGoal handle)
		{
		}

		public Transform m_transform;
		public AvatarIKGoal m_ikHandle;
		public float m_footDownMax;
		public float m_footOffset;
		public float m_footStepHeight;
		public float m_stabalizeDistance;
	}

	public bool m_footIK;
	public float m_footDownMax;
	public float m_footOffset;
	public float m_footStepHeight;
	public float m_stabalizeDistance;
	public bool m_useFeetValues;
	public Foot[] m_feets;
	public bool m_headRotation;
	public Transform[] m_eyes;
	public float m_lookWeight;
	public float m_bodyLookWeight;
	public float m_headLookWeight;
	public float m_eyeLookWeight;
	public float m_lookClamp;
	public Transform m_lookAt;
	public bool m_femaleHack;
	public Transform m_leftShoulder;
	public Transform m_rightShoulder;
	public float m_femaleOffset;
	public float m_maleOffset;
}
