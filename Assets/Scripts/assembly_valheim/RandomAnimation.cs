using UnityEngine;
using System;
using System.Collections.Generic;

public class RandomAnimation : MonoBehaviour
{
	[Serializable]
	public class RandomValue
	{
		public string m_name;
		public int m_values;
		public float m_interval;
		public bool m_floatValue;
		public float m_floatTransition;
	}

	public List<RandomAnimation.RandomValue> m_values;
}
