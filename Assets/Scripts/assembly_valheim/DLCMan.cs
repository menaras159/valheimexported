using UnityEngine;
using System;
using System.Collections.Generic;

public class DLCMan : MonoBehaviour
{
	[Serializable]
	public class DLCInfo
	{
		public string m_name;
		public uint[] m_steamAPPID;
	}

	public List<DLCMan.DLCInfo> m_dlcs;
}
