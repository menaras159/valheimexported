using UnityEngine;

public class SmokeSpawner : MonoBehaviour
{
	public GameObject m_smokePrefab;
	public float m_interval;
	public LayerMask m_testMask;
	public float m_testRadius;
}
