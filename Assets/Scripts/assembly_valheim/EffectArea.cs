using UnityEngine;

public class EffectArea : MonoBehaviour
{
	public enum Type
	{
		Heat = 1,
		Fire = 2,
		PlayerBase = 4,
		Burning = 8,
		Teleport = 16,
		NoMonsters = 32,
		WarmCozyArea = 64,
		None = 999,
	}

	public Type m_type;
	public string m_statusEffect;
}
