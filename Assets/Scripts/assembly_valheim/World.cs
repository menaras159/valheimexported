public class World
{
	public string m_name;
	public string m_seedName;
	public int m_seed;
	public long m_uid;
	public int m_worldGenVersion;
	public bool m_menu;
	public bool m_loadError;
	public bool m_versionError;
}
