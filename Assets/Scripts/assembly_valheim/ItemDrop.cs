using UnityEngine;
using System;
using System.Collections.Generic;

public class ItemDrop : MonoBehaviour
{
	[Serializable]
	public class ItemData
	{
		[Serializable]
		public class SharedData
		{
			public string m_name;
			public string m_dlc;
			public ItemDrop.ItemData.ItemType m_itemType;
			public Sprite[] m_icons;
			public ItemDrop.ItemData.ItemType m_attachOverride;
			[TextAreaAttribute]
			public string m_description;
			public int m_maxStackSize;
			public int m_maxQuality;
			public float m_weight;
			public int m_value;
			public bool m_teleportable;
			public bool m_questItem;
			public float m_equipDuration;
			public int m_variants;
			public Vector2Int m_trophyPos;
			public PieceTable m_buildPieces;
			public bool m_centerCamera;
			public string m_setName;
			public int m_setSize;
			public StatusEffect m_setStatusEffect;
			public StatusEffect m_equipStatusEffect;
			public float m_movementModifier;
			public float m_food;
			public float m_foodStamina;
			public float m_foodBurnTime;
			public float m_foodRegen;
			public Color m_foodColor;
			public Material m_armorMaterial;
			public bool m_helmetHideHair;
			public float m_armor;
			public float m_armorPerLevel;
			public List<HitData.DamageModPair> m_damageModifiers;
			public float m_blockPower;
			public float m_blockPowerPerLevel;
			public float m_deflectionForce;
			public float m_deflectionForcePerLevel;
			public float m_timedBlockBonus;
			public ItemDrop.ItemData.AnimationState m_animationState;
			public Skills.SkillType m_skillType;
			public int m_toolTier;
			public HitData.DamageTypes m_damages;
			public HitData.DamageTypes m_damagesPerLevel;
			public float m_attackForce;
			public float m_backstabBonus;
			public bool m_dodgeable;
			public bool m_blockable;
			public bool m_tamedOnly;
			public StatusEffect m_attackStatusEffect;
			public GameObject m_spawnOnHit;
			public GameObject m_spawnOnHitTerrain;
			public Attack m_attack;
			public Attack m_secondaryAttack;
			public bool m_useDurability;
			public bool m_destroyBroken;
			public bool m_canBeReparied;
			public float m_maxDurability;
			public float m_durabilityPerLevel;
			public float m_useDurabilityDrain;
			public float m_durabilityDrain;
			public float m_holdDurationMin;
			public float m_holdStaminaDrain;
			public string m_holdAnimationState;
			public string m_ammoType;
			public float m_aiAttackRange;
			public float m_aiAttackRangeMin;
			public float m_aiAttackInterval;
			public float m_aiAttackMaxAngle;
			public bool m_aiWhenFlying;
			public bool m_aiWhenWalking;
			public bool m_aiWhenSwiming;
			public bool m_aiPrioritized;
			public ItemDrop.ItemData.AiTarget m_aiTargetType;
			public EffectList m_hitEffect;
			public EffectList m_hitTerrainEffect;
			public EffectList m_blockEffect;
			public EffectList m_startEffect;
			public EffectList m_holdStartEffect;
			public EffectList m_triggerEffect;
			public EffectList m_trailStartEffect;
			public StatusEffect m_consumeStatusEffect;
		}

		public enum ItemType
		{
			None = 0,
			Material = 1,
			Consumable = 2,
			OneHandedWeapon = 3,
			Bow = 4,
			Shield = 5,
			Helmet = 6,
			Chest = 7,
			Ammo = 9,
			Customization = 10,
			Legs = 11,
			Hands = 12,
			Trophie = 13,
			TwoHandedWeapon = 14,
			Torch = 15,
			Misc = 16,
			Shoulder = 17,
			Utility = 18,
			Tool = 19,
			Attach_Atgeir = 20,
		}

		public enum AnimationState
		{
			Unarmed = 0,
			OneHanded = 1,
			TwoHandedClub = 2,
			Bow = 3,
			Shield = 4,
			Torch = 5,
			LeftTorch = 6,
			Atgeir = 7,
			TwoHandedAxe = 8,
			FishingRod = 9,
		}

		public enum AiTarget
		{
			Enemy = 0,
			FriendHurt = 1,
			Friend = 2,
		}

		public int m_stack;
		public float m_durability;
		public int m_quality;
		public int m_variant;
		public SharedData m_shared;
	}

	public bool m_autoPickup;
	public bool m_autoDestroy;
	public ItemData m_itemData;
}
