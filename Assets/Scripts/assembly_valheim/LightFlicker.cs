using UnityEngine;

public class LightFlicker : MonoBehaviour
{
	public float m_flickerIntensity;
	public float m_flickerSpeed;
	public float m_movement;
	public float m_ttl;
	public float m_fadeDuration;
	public float m_fadeInDuration;
}
