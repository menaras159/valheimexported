using UnityEngine;
using UnityEngine.UI;

public class StoreGui : MonoBehaviour
{
	public GameObject m_rootPanel;
	public Button m_buyButton;
	public Button m_sellButton;
	public RectTransform m_listRoot;
	public GameObject m_listElement;
	public Scrollbar m_listScroll;
	public ScrollRectEnsureVisible m_itemEnsureVisible;
	public Text m_coinText;
	public EffectList m_buyEffects;
	public EffectList m_sellEffects;
	public float m_hideDistance;
	public float m_itemSpacing;
	public ItemDrop m_coinPrefab;
}
