using UnityEngine;
using UnityEngine.UI;

public class SkillsDialog : MonoBehaviour
{
	public RectTransform m_listRoot;
	public GameObject m_elementPrefab;
	public Text m_totalSkillText;
	public float m_spacing;
}
