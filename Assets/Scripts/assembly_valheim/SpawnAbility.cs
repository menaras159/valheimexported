using UnityEngine;

public class SpawnAbility : MonoBehaviour
{
	public enum TargetType
	{
		ClosestEnemy = 0,
		RandomEnemy = 1,
		Caster = 2,
		Position = 3,
	}

	public GameObject[] m_spawnPrefab;
	public bool m_alertSpawnedCreature;
	public bool m_spawnAtTarget;
	public int m_minToSpawn;
	public int m_maxToSpawn;
	public int m_maxSpawned;
	public float m_spawnRadius;
	public bool m_snapToTerrain;
	public float m_spawnGroundOffset;
	public float m_spawnDelay;
	public TargetType m_targetType;
	public float m_maxTargetRange;
	public EffectList m_spawnEffects;
	public float m_projectileVelocity;
	public float m_projectileAccuracy;
}
