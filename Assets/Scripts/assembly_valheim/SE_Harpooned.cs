public class SE_Harpooned : StatusEffect
{
	public float m_pullForce;
	public float m_forcePower;
	public float m_pullSpeed;
	public float m_smoothDistance;
	public float m_maxLineSlack;
	public float m_breakDistance;
	public float m_maxDistance;
	public float m_staminaDrain;
	public float m_staminaDrainInterval;
}
