using UnityEngine;

public class Windmill : MonoBehaviour
{
	public Transform m_propeller;
	public Transform m_grindstone;
	public Transform m_bom;
	public AudioSource[] m_sfxLoops;
	public GameObject m_propellerAOE;
	public float m_minAOEPropellerSpeed;
	public float m_bomRotationSpeed;
	public float m_propellerRotationSpeed;
	public float m_grindstoneRotationSpeed;
	public float m_minWindSpeed;
	public float m_minPitch;
	public float m_maxPitch;
	public float m_maxPitchVel;
	public float m_maxVol;
	public float m_maxVolVel;
	public float m_audioChangeSpeed;
}
