using System;
using System.Collections.Generic;

[Serializable]
public class BiomeEnvSetup
{
	public string m_name;
	public Heightmap.Biome m_biome;
	public List<EnvEntry> m_environments;
	public string m_musicMorning;
	public string m_musicEvening;
	public string m_musicDay;
	public string m_musicNight;
}
