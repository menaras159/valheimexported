using UnityEngine;

public class OfferingBowl : MonoBehaviour
{
	public string m_name;
	public string m_useItemText;
	public ItemDrop m_bossItem;
	public int m_bossItems;
	public GameObject m_bossPrefab;
	public ItemDrop m_itemPrefab;
	public Transform m_itemSpawnPoint;
	public string m_setGlobalKey;
	public float m_spawnBossDelay;
	public float m_spawnBossMaxDistance;
	public float m_spawnBossMaxYDistance;
	public float m_spawnOffset;
	public bool m_useItemStands;
	public string m_itemStandPrefix;
	public float m_itemstandMaxRange;
	public EffectList m_fuelAddedEffects;
	public EffectList m_spawnBossStartEffects;
	public EffectList m_spawnBossDoneffects;
}
