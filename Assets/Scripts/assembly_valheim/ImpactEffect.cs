using UnityEngine;

public class ImpactEffect : MonoBehaviour
{
	public EffectList m_hitEffect;
	public EffectList m_destroyEffect;
	public float m_hitDestroyChance;
	public float m_minVelocity;
	public float m_maxVelocity;
	public bool m_damageToSelf;
	public bool m_damagePlayers;
	public bool m_damageFish;
	public int m_toolTier;
	public HitData.DamageTypes m_damages;
	public LayerMask m_triggerMask;
	public float m_interval;
}
