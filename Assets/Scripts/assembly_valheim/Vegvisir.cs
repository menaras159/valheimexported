using UnityEngine;

public class Vegvisir : MonoBehaviour
{
	public string m_name;
	public string m_locationName;
	public string m_pinName;
	public Minimap.PinType m_pinType;
}
