using UnityEngine;

public class MistEmitter : MonoBehaviour
{
	public float m_interval;
	public float m_totalRadius;
	public float m_testRadius;
	public int m_rays;
	public float m_placeOffset;
	public ParticleSystem m_psystem;
}
