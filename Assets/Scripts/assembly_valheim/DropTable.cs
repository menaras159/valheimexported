using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class DropTable
{
	[Serializable]
	public struct DropData
	{
		public GameObject m_item;
		public int m_stackMin;
		public int m_stackMax;
		public float m_weight;
	}

	public List<DropTable.DropData> m_drops;
	public int m_dropMin;
	public int m_dropMax;
	public float m_dropChance;
	public bool m_oneOfEach;
}
