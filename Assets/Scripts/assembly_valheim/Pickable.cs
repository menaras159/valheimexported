using UnityEngine;

public class Pickable : MonoBehaviour
{
	public GameObject m_hideWhenPicked;
	public GameObject m_itemPrefab;
	public int m_amount;
	public DropTable m_extraDrops;
	public string m_overrideName;
	public int m_respawnTimeMinutes;
	public float m_spawnOffset;
	public EffectList m_pickEffector;
	public bool m_pickEffectAtSpawnPoint;
	public bool m_useInteractAnimation;
	public bool m_tarPreventsPicking;
}
