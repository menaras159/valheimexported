using UnityEngine;
using System;
using System.Collections.Generic;

public class SpawnArea : MonoBehaviour
{
	[Serializable]
	public class SpawnData
	{
		public GameObject m_prefab;
		public float m_weight;
		public int m_maxLevel;
		public int m_minLevel;
	}

	public List<SpawnArea.SpawnData> m_prefabs;
	public float m_levelupChance;
	public float m_spawnIntervalSec;
	public float m_triggerDistance;
	public bool m_setPatrolSpawnPoint;
	public float m_spawnRadius;
	public float m_nearRadius;
	public float m_farRadius;
	public int m_maxNear;
	public int m_maxTotal;
	public bool m_onGroundOnly;
	public EffectList m_spawnEffects;
}
