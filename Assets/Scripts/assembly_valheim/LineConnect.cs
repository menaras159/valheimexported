using UnityEngine;

public class LineConnect : MonoBehaviour
{
	public bool m_centerOfCharacter;
	public string m_childObject;
	public bool m_hideIfNoConnection;
	public Vector3 m_noConnectionWorldOffset;
	public bool m_dynamicSlack;
	public float m_slack;
	public bool m_dynamicThickness;
	public float m_minDistance;
	public float m_maxDistance;
	public float m_minThickness;
	public float m_maxThickness;
	public float m_thicknessPower;
	public string m_netViewPrefix;
}
