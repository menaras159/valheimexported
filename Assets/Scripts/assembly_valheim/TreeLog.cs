using UnityEngine;

public class TreeLog : MonoBehaviour
{
	public float m_health;
	public HitData.DamageModifiers m_damages;
	public int m_minToolTier;
	public EffectList m_destroyedEffect;
	public EffectList m_hitEffect;
	public DropTable m_dropWhenDestroyed;
	public GameObject m_subLogPrefab;
	public Transform[] m_subLogPoints;
	public float m_spawnDistance;
	public float m_hitNoise;
}
