using UnityEngine;
using UnityEngine.UI;

public class Feedback : MonoBehaviour
{
	public Text m_subject;
	public Text m_text;
	public Button m_sendButton;
	public Toggle m_catBug;
	public Toggle m_catFeedback;
	public Toggle m_catIdea;
}
