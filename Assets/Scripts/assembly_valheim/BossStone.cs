using UnityEngine;

public class BossStone : MonoBehaviour
{
	public ItemStand m_itemStand;
	public GameObject m_activeEffect;
	public EffectList m_activateStep1;
	public EffectList m_activateStep2;
	public EffectList m_activateStep3;
	public string m_completedMessage;
	public MeshRenderer m_mesh;
	public int m_emissiveMaterialIndex;
	public Color m_activeEmissiveColor;
}
