using UnityEngine;
using UnityEngine.UI;

public class InventoryGrid : MonoBehaviour
{
	public GameObject m_elementPrefab;
	public RectTransform m_gridRoot;
	public Scrollbar m_scrollbar;
	public UIGroupHandler m_uiGroup;
	public float m_elementSpace;
}
