using UnityEngine;
using UnityEngine.UI;

public class MessageHud : MonoBehaviour
{
	public enum MessageType
	{
		TopLeft = 1,
		Center = 2,
	}

	public Text m_messageText;
	public Image m_messageIcon;
	public Text m_messageCenterText;
	public GameObject m_unlockMsgPrefab;
	public int m_maxUnlockMsgSpace;
	public int m_maxUnlockMessages;
	public int m_maxLogMessages;
	public GameObject m_biomeFoundPrefab;
	public GameObject m_biomeFoundStinger;
}
