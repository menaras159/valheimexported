using UnityEngine;
using System;
using System.Collections.Generic;

public class ZoneSystem : MonoBehaviour
{
	[Serializable]
	public class ZoneVegetation
	{
		public string m_name;
		public GameObject m_prefab;
		public bool m_enable;
		public float m_min;
		public float m_max;
		public bool m_forcePlacement;
		public float m_scaleMin;
		public float m_scaleMax;
		public float m_randTilt;
		public float m_chanceToUseGroundTilt;
		public Heightmap.Biome m_biome;
		public Heightmap.BiomeArea m_biomeArea;
		public bool m_blockCheck;
		public float m_minAltitude;
		public float m_maxAltitude;
		public float m_minOceanDepth;
		public float m_maxOceanDepth;
		public float m_minTilt;
		public float m_maxTilt;
		public float m_terrainDeltaRadius;
		public float m_maxTerrainDelta;
		public float m_minTerrainDelta;
		public bool m_snapToWater;
		public float m_groundOffset;
		public int m_groupSizeMin;
		public int m_groupSizeMax;
		public float m_groupRadius;
		public bool m_inForest;
		public float m_forestTresholdMin;
		public float m_forestTresholdMax;
		public bool m_foldout;
	}

	[Serializable]
	public class ZoneLocation
	{
		public bool m_enable;
		public string m_prefabName;
		public Heightmap.Biome m_biome;
		public Heightmap.BiomeArea m_biomeArea;
		public int m_quantity;
		public float m_chanceToSpawn;
		public bool m_prioritized;
		public bool m_centerFirst;
		public bool m_unique;
		public string m_group;
		public float m_minDistanceFromSimilar;
		public bool m_iconAlways;
		public bool m_iconPlaced;
		public bool m_randomRotation;
		public bool m_slopeRotation;
		public bool m_snapToWater;
		public float m_maxTerrainDelta;
		public float m_minTerrainDelta;
		public bool m_inForest;
		public float m_forestTresholdMin;
		public float m_forestTresholdMax;
		public float m_minDistance;
		public float m_maxDistance;
		public float m_minAltitude;
		public float m_maxAltitude;
		public bool m_foldout;
	}

	public List<Heightmap.Biome> m_biomeFolded;
	public List<Heightmap.Biome> m_vegetationFolded;
	public List<Heightmap.Biome> m_locationFolded;
	public int m_activeArea;
	public int m_activeDistantArea;
	public float m_zoneSize;
	public float m_zoneTTL;
	public float m_zoneTTS;
	public GameObject m_zonePrefab;
	public GameObject m_zoneCtrlPrefab;
	public GameObject m_locationProxyPrefab;
	public float m_waterLevel;
	public int m_pgwVersion;
	public int m_locationVersion;
	public List<string> m_locationScenes;
	public List<ZoneSystem.ZoneVegetation> m_vegetation;
	public List<ZoneSystem.ZoneLocation> m_locations;
}
