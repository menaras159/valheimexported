using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class Minimap : MonoBehaviour
{
	[Serializable]
	public struct SpriteData
	{
		public Minimap.PinType m_name;
		public Sprite m_icon;
	}

	[Serializable]
	public struct LocationSpriteData
	{
		public string m_name;
		public Sprite m_icon;
	}

	public enum PinType
	{
		Icon0 = 0,
		Icon1 = 1,
		Icon2 = 2,
		Icon3 = 3,
		Death = 4,
		Bed = 5,
		Icon4 = 6,
		Shout = 7,
		None = 8,
		Boss = 9,
		Player = 10,
		RandomEvent = 11,
		Ping = 12,
		EventArea = 13,
	}

	public GameObject m_smallRoot;
	public GameObject m_largeRoot;
	public RawImage m_mapImageSmall;
	public RawImage m_mapImageLarge;
	public RectTransform m_pinRootSmall;
	public RectTransform m_pinRootLarge;
	public Text m_biomeNameSmall;
	public Text m_biomeNameLarge;
	public RectTransform m_smallShipMarker;
	public RectTransform m_largeShipMarker;
	public RectTransform m_smallMarker;
	public RectTransform m_largeMarker;
	public RectTransform m_windMarker;
	public RectTransform m_gamepadCrosshair;
	public Toggle m_publicPosition;
	public Image m_selectedIcon0;
	public Image m_selectedIcon1;
	public Image m_selectedIcon2;
	public Image m_selectedIcon3;
	public Image m_selectedIcon4;
	public Image m_selectedIconDeath;
	public Image m_selectedIconBoss;
	public GameObject m_mapSmall;
	public GameObject m_mapLarge;
	public GameObject m_pinPrefab;
	public InputField m_nameInput;
	public int m_textureSize;
	public float m_pixelSize;
	public float m_minZoom;
	public float m_maxZoom;
	public float m_showNamesZoom;
	public float m_exploreInterval;
	public float m_exploreRadius;
	public float m_removeRadius;
	public float m_pinSizeSmall;
	public float m_pinSizeLarge;
	public float m_clickDuration;
	public List<Minimap.SpriteData> m_icons;
	public List<Minimap.LocationSpriteData> m_locationIcons;
	public Color m_meadowsColor;
	public Color m_ashlandsColor;
	public Color m_blackforestColor;
	public Color m_deepnorthColor;
	public Color m_heathColor;
	public Color m_swampColor;
	public Color m_mountainColor;
	public Color m_mistlandsColor;
	public GameObject m_sharedMapHint;
	public List<GameObject> m_hints;
}
