using UnityEngine;

public class WayStone : MonoBehaviour
{
	[TextAreaAttribute]
	public string m_activateMessage;
	public GameObject m_activeObject;
	public EffectList m_activeEffect;
}
