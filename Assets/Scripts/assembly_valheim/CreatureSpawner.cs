using UnityEngine;

public class CreatureSpawner : MonoBehaviour
{
	public GameObject m_creaturePrefab;
	public int m_maxLevel;
	public int m_minLevel;
	public float m_respawnTimeMinuts;
	public float m_triggerDistance;
	public float m_triggerNoise;
	public bool m_spawnAtNight;
	public bool m_spawnAtDay;
	public bool m_requireSpawnArea;
	public bool m_spawnInPlayerBase;
	public bool m_setPatrolSpawnPoint;
	public EffectList m_spawnEffects;
}
