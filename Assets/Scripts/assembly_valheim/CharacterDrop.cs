using UnityEngine;
using System;
using System.Collections.Generic;

public class CharacterDrop : MonoBehaviour
{
	[Serializable]
	public class Drop
	{
		public GameObject m_prefab;
		public int m_amountMin;
		public int m_amountMax;
		public float m_chance;
		public bool m_onePerPlayer;
		public bool m_levelMultiplier;
	}

	public Vector3 m_spawnOffset;
	public List<CharacterDrop.Drop> m_drops;
}
