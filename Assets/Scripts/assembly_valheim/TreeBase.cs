using UnityEngine;

public class TreeBase : MonoBehaviour
{
	public float m_health;
	public HitData.DamageModifiers m_damageModifiers;
	public int m_minToolTier;
	public EffectList m_destroyedEffect;
	public EffectList m_hitEffect;
	public EffectList m_respawnEffect;
	public GameObject m_trunk;
	public GameObject m_stubPrefab;
	public GameObject m_logPrefab;
	public Transform m_logSpawnPoint;
	public DropTable m_dropWhenDestroyed;
	public float m_spawnYOffset;
	public float m_spawnYStep;
}
