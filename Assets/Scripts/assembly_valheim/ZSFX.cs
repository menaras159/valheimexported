using UnityEngine;

public class ZSFX : MonoBehaviour
{
	public bool m_playOnAwake;
	public AudioClip[] m_audioClips;
	public float m_maxPitch;
	public float m_minPitch;
	public float m_maxVol;
	public float m_minVol;
	public float m_fadeInDuration;
	public float m_fadeOutDuration;
	public float m_fadeOutDelay;
	public bool m_fadeOutOnAwake;
	public bool m_randomPan;
	public float m_minPan;
	public float m_maxPan;
	public float m_maxDelay;
	public float m_minDelay;
	public bool m_distanceReverb;
	public bool m_useCustomReverbDistance;
	public float m_customReverbDistance;
}
