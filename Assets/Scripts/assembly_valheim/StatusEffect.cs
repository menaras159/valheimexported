using UnityEngine;

public class StatusEffect : ScriptableObject
{
	public enum StatusAttribute
	{
		None = 0,
		ColdResistance = 1,
		DoubleImpactDamage = 2,
		SailingPower = 4,
	}

	public string m_name;
	public string m_category;
	public Sprite m_icon;
	public bool m_flashIcon;
	public bool m_cooldownIcon;
	[TextAreaAttribute]
	public string m_tooltip;
	public StatusAttribute m_attributes;
	public MessageHud.MessageType m_startMessageType;
	public string m_startMessage;
	public MessageHud.MessageType m_stopMessageType;
	public string m_stopMessage;
	public MessageHud.MessageType m_repeatMessageType;
	public string m_repeatMessage;
	public float m_repeatInterval;
	public float m_ttl;
	public EffectList m_startEffects;
	public EffectList m_stopEffects;
	public float m_cooldown;
	public string m_activationAnimation;
}
