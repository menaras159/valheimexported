using UnityEngine;

public class ShipEffects : MonoBehaviour
{
	public Transform m_shadow;
	public float m_offset;
	public float m_minimumWakeVel;
	public GameObject m_speedWakeRoot;
	public GameObject m_wakeSoundRoot;
	public GameObject m_inWaterSoundRoot;
	public float m_audioFadeDuration;
	public AudioSource m_sailSound;
	public float m_sailFadeDuration;
	public GameObject m_splashEffects;
}
