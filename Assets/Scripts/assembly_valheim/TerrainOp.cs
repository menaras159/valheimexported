using UnityEngine;
using System;

public class TerrainOp : MonoBehaviour
{
	[Serializable]
	public class Settings
	{
		public float m_levelOffset;
		public bool m_level;
		public float m_levelRadius;
		public bool m_square;
		public bool m_raise;
		public float m_raiseRadius;
		public float m_raisePower;
		public float m_raiseDelta;
		public bool m_smooth;
		public float m_smoothRadius;
		public float m_smoothPower;
		public bool m_paintCleared;
		public bool m_paintHeightCheck;
		public TerrainModifier.PaintType m_paintType;
		public float m_paintRadius;
	}

	public Settings m_settings;
	public EffectList m_onPlacedEffect;
	public GameObject m_spawnOnPlaced;
	public float m_chanceToSpawn;
	public int m_maxSpawned;
	public bool m_spawnAtMaxLevelDepth;
}
