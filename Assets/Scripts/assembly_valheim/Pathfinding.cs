using UnityEngine;

public class Pathfinding : MonoBehaviour
{
	public enum AgentType
	{
		Humanoid = 1,
		TrollSize = 2,
		HugeSize = 3,
		HorseSize = 4,
		HumanoidNoSwim = 5,
		HumanoidAvoidWater = 6,
		Fish = 7,
		Wolf = 8,
		BigFish = 9,
		GoblinBruteSize = 10,
		HumanoidBigNoSwim = 11,
	}

	public LayerMask m_layers;
	public LayerMask m_waterLayers;
	public float m_tileSize;
	public float m_defaultCost;
	public float m_waterCost;
	public float m_linkCost;
	public float m_linkWidth;
	public float m_updateInterval;
	public float m_tileTimeout;
}
