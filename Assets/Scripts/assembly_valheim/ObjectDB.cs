using UnityEngine;
using System.Collections.Generic;

public class ObjectDB : MonoBehaviour
{
	public List<StatusEffect> m_StatusEffects;
	public List<GameObject> m_items;
	public List<Recipe> m_recipes;
}
