using UnityEngine;

public class ZNet : MonoBehaviour
{
	public int m_hostPort;
	public RectTransform m_passwordDialog;
	public RectTransform m_connectingDialog;
	public float m_badConnectionPing;
	public int m_zdoSectorsWidth;
	public int m_serverPlayerLimit;
}
