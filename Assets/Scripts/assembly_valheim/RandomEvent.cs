using System;
using System.Collections.Generic;

[Serializable]
public class RandomEvent
{
	public string m_name;
	public bool m_enabled;
	public bool m_random;
	public float m_duration;
	public bool m_nearBaseOnly;
	public bool m_pauseIfNoPlayerInArea;
	public Heightmap.Biome m_biome;
	public List<string> m_requiredGlobalKeys;
	public List<string> m_notRequiredGlobalKeys;
	public string m_startMessage;
	public string m_endMessage;
	public string m_forceMusic;
	public string m_forceEnvironment;
	public List<SpawnSystem.SpawnData> m_spawn;
}
