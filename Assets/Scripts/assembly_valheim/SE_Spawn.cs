using UnityEngine;

public class SE_Spawn : StatusEffect
{
	public float m_delay;
	public GameObject m_prefab;
	public Vector3 m_spawnOffset;
	public EffectList m_spawnEffect;
}
