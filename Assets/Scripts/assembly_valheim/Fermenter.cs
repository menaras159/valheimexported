using UnityEngine;
using System;
using System.Collections.Generic;

public class Fermenter : MonoBehaviour
{
	[Serializable]
	public class ItemConversion
	{
		public ItemDrop m_from;
		public ItemDrop m_to;
		public int m_producedItems;
	}

	public string m_name;
	public float m_fermentationDuration;
	public GameObject m_fermentingObject;
	public GameObject m_readyObject;
	public GameObject m_topObject;
	public EffectList m_addedEffects;
	public EffectList m_tapEffects;
	public EffectList m_spawnEffects;
	public Switch m_addSwitch;
	public Switch m_tapSwitch;
	public float m_tapDelay;
	public Transform m_outputPoint;
	public Transform m_roofCheckPoint;
	public List<Fermenter.ItemConversion> m_conversion;
}
