using UnityEngine;
using System.Collections.Generic;

public class EnvMan : MonoBehaviour
{
	public Light m_dirLight;
	public bool m_debugTimeOfDay;
	public float m_debugTime;
	public string m_debugEnv;
	public bool m_debugWind;
	public float m_debugWindAngle;
	public float m_debugWindIntensity;
	public float m_sunHorizonTransitionH;
	public float m_sunHorizonTransitionL;
	public long m_dayLengthSec;
	public float m_transitionDuration;
	public long m_environmentDuration;
	public long m_windPeriodDuration;
	public float m_windTransitionDuration;
	public List<EnvSetup> m_environments;
	public List<BiomeEnvSetup> m_biomes;
	public string m_introEnvironment;
	public float m_edgeOfWorldWidth;
	public float m_randomMusicIntervalMin;
	public float m_randomMusicIntervalMax;
	public MeshRenderer m_clouds;
	public MeshRenderer m_rainClouds;
	public MeshRenderer m_rainCloudsDownside;
	public float m_wetTransitionDuration;
	public double m_sleepCooldownSeconds;
}
