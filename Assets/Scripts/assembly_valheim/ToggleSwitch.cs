using UnityEngine;

public class ToggleSwitch : MonoBehaviour
{
	public MeshRenderer m_renderer;
	public Material m_enableMaterial;
	public Material m_disableMaterial;
	public string m_hoverText;
	public string m_name;
}
