using UnityEngine;

public class StationExtension : MonoBehaviour
{
	public CraftingStation m_craftingStation;
	public float m_maxStationDistance;
	public GameObject m_connectionPrefab;
	public Vector3 m_connectionOffset;
}
