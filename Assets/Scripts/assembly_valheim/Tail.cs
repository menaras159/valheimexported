using UnityEngine;
using System.Collections.Generic;

public class Tail : MonoBehaviour
{
	public List<Transform> m_tailJoints;
	public float m_yMovementDistance;
	public float m_yMovementFreq;
	public float m_yMovementOffset;
	public float m_maxAngle;
	public float m_gravity;
	public float m_gravityInWater;
	public bool m_waterSurfaceCheck;
	public bool m_groundCheck;
	public float m_smoothness;
	public float m_tailRadius;
	public Character m_character;
	public Rigidbody m_characterBody;
	public Rigidbody m_tailBody;
}
