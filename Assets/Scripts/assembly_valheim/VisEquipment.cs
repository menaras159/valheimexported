using UnityEngine;
using System;

public class VisEquipment : MonoBehaviour
{
	[Serializable]
	public class PlayerModel
	{
		public Mesh m_mesh;
		public Material m_baseMaterial;
	}

	public SkinnedMeshRenderer m_bodyModel;
	public Transform m_leftHand;
	public Transform m_rightHand;
	public Transform m_helmet;
	public Transform m_backShield;
	public Transform m_backMelee;
	public Transform m_backTwohandedMelee;
	public Transform m_backBow;
	public Transform m_backTool;
	public Transform m_backAtgeir;
	public CapsuleCollider[] m_clothColliders;
	public PlayerModel[] m_models;
	public bool m_isPlayer;
	public bool m_useAllTrails;
}
