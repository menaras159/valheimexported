public class ZDO
{
	public enum ObjectType
	{
		Default = 0,
		Prioritized = 1,
		Solid = 2,
		Terrain = 3,
	}

	public bool m_persistent;
	public bool m_distant;
	public long m_owner;
	public long m_timeCreated;
	public uint m_ownerRevision;
	public uint m_dataRevision;
	public int m_pgwVersion;
	public ObjectType m_type;
	public float m_tempSortValue;
	public bool m_tempHaveRevision;
	public int m_tempRemoveEarmark;
	public int m_tempCreateEarmark;
}
