using UnityEngine;

public class Heightmap : MonoBehaviour
{
	public enum Biome
	{
		None = 0,
		Meadows = 1,
		Swamp = 2,
		Mountain = 4,
		BlackForest = 8,
		Plains = 16,
		AshLands = 32,
		DeepNorth = 64,
		Ocean = 256,
		Mistlands = 512,
		BiomesMax = 513,
	}

	public enum BiomeArea
	{
		Edge = 1,
		Median = 2,
		Everything = 3,
	}

	public GameObject m_terrainCompilerPrefab;
	public int m_width;
	public float m_scale;
	public Material m_material;
	public bool m_isDistantLod;
	public bool m_distantLodEditorHax;
}
