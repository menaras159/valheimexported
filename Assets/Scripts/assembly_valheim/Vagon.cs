using UnityEngine;
using System;
using System.Collections.Generic;

public class Vagon : MonoBehaviour
{
	[Serializable]
	public class LoadData
	{
		public GameObject m_gameobject;
		public float m_minPercentage;
	}

	public Transform m_attachPoint;
	public string m_name;
	public float m_detachDistance;
	public Vector3 m_attachOffset;
	public Container m_container;
	public Transform m_lineAttachPoints0;
	public Transform m_lineAttachPoints1;
	public Vector3 m_lineAttachOffset;
	public float m_breakForce;
	public float m_spring;
	public float m_springDamping;
	public float m_baseMass;
	public float m_itemWeightMassFactor;
	public AudioSource[] m_wheelLoops;
	public float m_minPitch;
	public float m_maxPitch;
	public float m_maxPitchVel;
	public float m_maxVol;
	public float m_maxVolVel;
	public float m_audioChangeSpeed;
	public Rigidbody[] m_wheels;
	public List<Vagon.LoadData> m_loadVis;
}
