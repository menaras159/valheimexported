using UnityEngine;

public class ZNetView : MonoBehaviour
{
	public bool m_persistent;
	public bool m_distant;
	public ZDO.ObjectType m_type;
	public bool m_syncInitialScale;
}
