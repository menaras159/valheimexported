using UnityEngine;

public class Location : MonoBehaviour
{
	public float m_exteriorRadius;
	public bool m_noBuild;
	public bool m_clearArea;
	public bool m_applyRandomDamage;
	public bool m_hasInterior;
	public float m_interiorRadius;
	public string m_interiorEnvironment;
	public GameObject m_interiorPrefab;
}
