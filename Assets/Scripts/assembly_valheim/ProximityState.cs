using UnityEngine;

public class ProximityState : MonoBehaviour
{
	public bool m_playerOnly;
	public Animator m_animator;
	public EffectList m_movingClose;
	public EffectList m_movingAway;
}
