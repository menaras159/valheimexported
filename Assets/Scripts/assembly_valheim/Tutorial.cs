using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
	[Serializable]
	public class TutorialText
	{
		public string m_name;
		public string m_topic;
		public string m_label;
		[TextAreaAttribute]
		public string m_text;
	}

	public List<Tutorial.TutorialText> m_texts;
	public RectTransform m_windowRoot;
	public Text m_topic;
	public Text m_text;
	public GameObject m_ravenPrefab;
}
