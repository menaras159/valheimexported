using UnityEngine;
using System;
using System.Collections.Generic;

public class LevelEffects : MonoBehaviour
{
	[Serializable]
	public class LevelSetup
	{
		public float m_scale;
		public float m_hue;
		public float m_saturation;
		public float m_value;
		public GameObject m_enableObject;
	}

	public Renderer m_mainRender;
	public GameObject m_baseEnableObject;
	public List<LevelEffects.LevelSetup> m_levelSetups;
}
