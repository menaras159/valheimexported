using UnityEngine;
using System.Collections.Generic;

public class ItemStand : MonoBehaviour
{
	public ZNetView m_netViewOverride;
	public string m_name;
	public Transform m_attachOther;
	public Transform m_dropSpawnPoint;
	public bool m_canBeRemoved;
	public bool m_autoAttach;
	public List<ItemDrop.ItemData.ItemType> m_supportedTypes;
	public List<ItemDrop> m_unsupportedItems;
	public List<ItemDrop> m_supportedItems;
	public EffectList m_effects;
	public EffectList m_destroyEffects;
	public float m_powerActivationDelay;
	public StatusEffect m_guardianPower;
	public EffectList m_activatePowerEffects;
	public EffectList m_activatePowerEffectsPlayer;
}
