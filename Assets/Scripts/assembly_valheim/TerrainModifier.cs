using UnityEngine;

public class TerrainModifier : MonoBehaviour
{
	public enum PaintType
	{
		Dirt = 0,
		Cultivate = 1,
		Paved = 2,
		Reset = 3,
	}

	public int m_sortOrder;
	public bool m_useTerrainCompiler;
	public bool m_playerModifiction;
	public float m_levelOffset;
	public bool m_level;
	public float m_levelRadius;
	public bool m_square;
	public bool m_smooth;
	public float m_smoothRadius;
	public float m_smoothPower;
	public bool m_paintCleared;
	public bool m_paintHeightCheck;
	public PaintType m_paintType;
	public float m_paintRadius;
	public EffectList m_onPlacedEffect;
	public GameObject m_spawnOnPlaced;
	public float m_chanceToSpawn;
	public int m_maxSpawned;
	public bool m_spawnAtMaxLevelDepth;
}
