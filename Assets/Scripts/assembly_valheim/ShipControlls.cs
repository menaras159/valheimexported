using UnityEngine;

public class ShipControlls : MonoBehaviour
{
	public string m_hoverText;
	public Ship m_ship;
	public float m_maxUseRange;
	public Transform m_attachPoint;
	public Vector3 m_detachOffset;
	public string m_attachAnimation;
}
