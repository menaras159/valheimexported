using System;
using UnityEngine;

public class Humanoid : Character
{
	[Serializable]
	public class ItemSet
	{
		public string m_name;
		public GameObject[] m_items;
	}

	public float m_equipStaminaDrain;
	public float m_blockStaminaDrain;
	public GameObject[] m_defaultItems;
	public GameObject[] m_randomWeapon;
	public GameObject[] m_randomArmor;
	public GameObject[] m_randomShield;
	public ItemSet[] m_randomSets;
	public ItemDrop m_unarmedWeapon;
	public EffectList m_pickupEffects;
	public EffectList m_dropEffects;
	public EffectList m_consumeItemEffects;
	public EffectList m_equipEffects;
	public EffectList m_perfectBlockEffect;
}
