using UnityEngine;

public class TeleportWorld : MonoBehaviour
{
	public float m_activationRange;
	public float m_exitDistance;
	public Transform m_proximityRoot;
	public Color m_colorUnconnected;
	public Color m_colorTargetfound;
	public EffectFade m_target_found;
	public MeshRenderer m_model;
	public EffectList m_connected;
}
