using UnityEngine;

public class Ship : MonoBehaviour
{
	public GameObject m_sailObject;
	public GameObject m_mastObject;
	public GameObject m_rudderObject;
	public ShipControlls m_shipControlls;
	public Transform m_controlGuiPos;
	public BoxCollider m_floatCollider;
	public float m_waterLevelOffset;
	public float m_forceDistance;
	public float m_force;
	public float m_damping;
	public float m_dampingSideway;
	public float m_dampingForward;
	public float m_angularDamping;
	public float m_disableLevel;
	public float m_sailForceOffset;
	public float m_sailForceFactor;
	public float m_rudderSpeed;
	public float m_stearForceOffset;
	public float m_stearForce;
	public float m_stearVelForceFactor;
	public float m_backwardForce;
	public float m_rudderRotationMax;
	public float m_rudderRotationSpeed;
	public float m_minWaterImpactForce;
	public float m_minWaterImpactInterval;
	public float m_waterImpactDamage;
	public float m_upsideDownDmgInterval;
	public float m_upsideDownDmg;
	public EffectList m_waterImpactEffect;
}
