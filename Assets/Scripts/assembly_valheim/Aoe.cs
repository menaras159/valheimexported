using UnityEngine;

public class Aoe : MonoBehaviour
{
	public bool m_useAttackSettings;
	public HitData.DamageTypes m_damage;
	public bool m_dodgeable;
	public bool m_blockable;
	public int m_toolTier;
	public float m_attackForce;
	public float m_backstabBonus;
	public string m_statusEffect;
	public HitData.DamageTypes m_damagePerLevel;
	public bool m_attackForceForward;
	public float m_damageSelf;
	public bool m_hitOwner;
	public bool m_hitParent;
	public bool m_hitSame;
	public bool m_hitFriendly;
	public bool m_hitEnemy;
	public bool m_hitCharacters;
	public bool m_hitProps;
	public Skills.SkillType m_skill;
	public bool m_useTriggers;
	public bool m_triggerEnterOnly;
	public BoxCollider m_useCollider;
	public float m_radius;
	public float m_ttl;
	public float m_hitInterval;
	public EffectList m_hitEffects;
	public bool m_attachToCaster;
}
