using UnityEngine;

public class ReflectionUpdate : MonoBehaviour
{
	public ReflectionProbe m_probe1;
	public ReflectionProbe m_probe2;
	public float m_interval;
	public float m_reflectionHeight;
	public float m_transitionDuration;
	public float m_power;
}
