using System;
using UnityEngine;

public class Piece : StaticTarget
{
	[Serializable]
	public class Requirement
	{
		public ItemDrop m_resItem;
		public int m_amount;
		public int m_amountPerLevel;
		public bool m_recover;
	}

	public enum PieceCategory
	{
		Misc = 0,
		Crafting = 1,
		Building = 2,
		Furniture = 3,
		Max = 4,
		All = 100,
	}

	public enum ComfortGroup
	{
		None = 0,
		Fire = 1,
		Bed = 2,
		Banner = 3,
		Chair = 4,
		Table = 5,
	}

	public bool m_targetNonPlayerBuilt;
	public Sprite m_icon;
	public string m_name;
	public string m_description;
	public bool m_enabled;
	public PieceCategory m_category;
	public bool m_isUpgrade;
	public int m_comfort;
	public ComfortGroup m_comfortGroup;
	public GameObject m_comfortObject;
	public bool m_groundPiece;
	public bool m_allowAltGroundPlacement;
	public bool m_groundOnly;
	public bool m_cultivatedGroundOnly;
	public bool m_waterPiece;
	public bool m_clipGround;
	public bool m_clipEverything;
	public bool m_noInWater;
	public bool m_notOnWood;
	public bool m_notOnTiltingSurface;
	public bool m_inCeilingOnly;
	public bool m_notOnFloor;
	public bool m_noClipping;
	public bool m_onlyInTeleportArea;
	public bool m_allowedInDungeons;
	public float m_spaceRequirement;
	public bool m_repairPiece;
	public bool m_canBeRemoved;
	public bool m_allowRotatedOverlap;
	public Heightmap.Biome m_onlyInBiome;
	public EffectList m_placeEffect;
	public string m_dlc;
	public CraftingStation m_craftingStation;
	public Requirement[] m_resources;
	public GameObject m_destroyedLootPrefab;
}
