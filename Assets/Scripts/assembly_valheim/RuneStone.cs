using UnityEngine;
using System;
using System.Collections.Generic;

public class RuneStone : MonoBehaviour
{
	[Serializable]
	public class RandomRuneText
	{
		public string m_topic;
		public string m_label;
		public string m_text;
	}

	public string m_name;
	public string m_topic;
	public string m_label;
	public string m_text;
	public List<RuneStone.RandomRuneText> m_randomTexts;
	public string m_locationName;
	public string m_pinName;
	public Minimap.PinType m_pinType;
	public bool m_showMap;
}
