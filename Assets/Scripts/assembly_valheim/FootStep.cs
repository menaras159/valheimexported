using UnityEngine;
using System;
using System.Collections.Generic;

public class FootStep : MonoBehaviour
{
	[Serializable]
	public class StepEffect
	{
		public string m_name;
		public FootStep.MotionType m_motionType;
		public FootStep.GroundMaterial m_material;
		public GameObject[] m_effectPrefabs;
	}

	public enum MotionType
	{
		Jog = 1,
		Run = 2,
		Sneak = 4,
		Climbing = 8,
		Swiming = 16,
		Land = 32,
		Walk = 64,
	}

	public enum GroundMaterial
	{
		None = 0,
		Default = 1,
		Water = 2,
		Stone = 4,
		Wood = 8,
		Snow = 16,
		Mud = 32,
		Grass = 64,
		GenericGround = 128,
		Metal = 256,
		Tar = 512,
	}

	public float m_footstepCullDistance;
	public List<FootStep.StepEffect> m_effects;
	public Transform[] m_feet;
}
