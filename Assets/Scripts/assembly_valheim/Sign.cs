using UnityEngine;
using UnityEngine.UI;

public class Sign : MonoBehaviour
{
	public Text m_textWidget;
	public string m_name;
	public string m_defaultText;
	public int m_characterLimit;
}
