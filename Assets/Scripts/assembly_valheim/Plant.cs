using UnityEngine;

public class Plant : SlowUpdate
{
	public string m_name;
	public float m_growTime;
	public float m_growTimeMax;
	public GameObject[] m_grownPrefabs;
	public float m_minScale;
	public float m_maxScale;
	public float m_growRadius;
	public bool m_needCultivatedGround;
	public bool m_destroyIfCantGrow;
	[SerializeField]
	private GameObject m_healthy;
	[SerializeField]
	private GameObject m_unhealthy;
	[SerializeField]
	private GameObject m_healthyGrown;
	[SerializeField]
	private GameObject m_unhealthyGrown;
	public Heightmap.Biome m_biome;
	public EffectList m_growEffect;
}
