using UnityEngine;

public class DistantFogEmitter : MonoBehaviour
{
	public float m_interval;
	public float m_minRadius;
	public float m_maxRadius;
	public float m_mountainSpawnChance;
	public float m_landSpawnChance;
	public float m_waterSpawnChance;
	public float m_mountainLimit;
	public float m_emitStep;
	public int m_emitPerStep;
	public int m_particles;
	public float m_placeOffset;
	public ParticleSystem[] m_psystems;
	public bool m_skipWater;
}
