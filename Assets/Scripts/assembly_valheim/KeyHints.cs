using UnityEngine;

public class KeyHints : MonoBehaviour
{
	public GameObject m_buildHints;
	public GameObject m_combatHints;
	public GameObject m_primaryAttackGP;
	public GameObject m_primaryAttackKB;
	public GameObject m_secondaryAttackGP;
	public GameObject m_secondaryAttackKB;
	public GameObject m_bowDrawGP;
	public GameObject m_bowDrawKB;
}
