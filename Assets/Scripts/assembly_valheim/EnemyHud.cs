using UnityEngine;

public class EnemyHud : MonoBehaviour
{
	public GameObject m_hudRoot;
	public GameObject m_baseHud;
	public GameObject m_baseHudBoss;
	public GameObject m_baseHudPlayer;
	public GameObject m_baseHudMount;
	public float m_maxShowDistance;
	public float m_maxShowDistanceBoss;
	public float m_hoverShowDuration;
}
