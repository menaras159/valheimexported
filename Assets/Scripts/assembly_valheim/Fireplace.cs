using UnityEngine;

public class Fireplace : MonoBehaviour
{
	public string m_name;
	public float m_startFuel;
	public float m_maxFuel;
	public float m_secPerFuel;
	public float m_checkTerrainOffset;
	public float m_coverCheckOffset;
	public float m_holdRepeatInterval;
	public GameObject m_enabledObject;
	public GameObject m_enabledObjectLow;
	public GameObject m_enabledObjectHigh;
	public GameObject m_playerBaseObject;
	public ItemDrop m_fuelItem;
	public SmokeSpawner m_smokeSpawner;
	public EffectList m_fuelAddedEffects;
	public ItemDrop m_fireworkItem;
	public int m_fireworkItems;
	public GameObject m_fireworks;
}
