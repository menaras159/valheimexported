using UnityEngine;

public class Thunder : MonoBehaviour
{
	public float m_strikeIntervalMin;
	public float m_strikeIntervalMax;
	public float m_thunderDelayMin;
	public float m_thunderDelayMax;
	public float m_flashDistanceMin;
	public float m_flashDistanceMax;
	public float m_flashAltitude;
	public EffectList m_flashEffect;
	public EffectList m_thunderEffect;
	public bool m_spawnThor;
	public string m_requiredGlobalKey;
	public GameObject m_thorPrefab;
	public float m_thorSpawnDistance;
	public float m_thorSpawnAltitudeMax;
	public float m_thorSpawnAltitudeMin;
	public float m_thorInterval;
	public float m_thorChance;
}
