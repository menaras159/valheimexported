using UnityEngine;

public class RandomFlyingBird : MonoBehaviour
{
	public float m_flyRange;
	public float m_minAlt;
	public float m_maxAlt;
	public float m_speed;
	public float m_turnRate;
	public float m_wpDuration;
	public float m_flapDuration;
	public float m_sailDuration;
	public float m_landChance;
	public float m_landDuration;
	public float m_avoidDangerDistance;
	public bool m_noRandomFlightAtNight;
	public float m_randomNoiseIntervalMin;
	public float m_randomNoiseIntervalMax;
	public bool m_noNoiseAtNight;
	public EffectList m_randomNoise;
	public GameObject m_flyingModel;
	public GameObject m_landedModel;
}
