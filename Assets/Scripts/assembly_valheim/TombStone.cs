using UnityEngine;
using UnityEngine.UI;

public class TombStone : MonoBehaviour
{
	public string m_text;
	public GameObject m_floater;
	public Text m_worldText;
	public float m_spawnUpVel;
	public StatusEffect m_lootStatusEffect;
	public EffectList m_removeEffect;
}
