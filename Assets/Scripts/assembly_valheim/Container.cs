using UnityEngine;

public class Container : MonoBehaviour
{
	public enum PrivacySetting
	{
		Private = 0,
		Group = 1,
		Public = 2,
	}

	public string m_name;
	public Sprite m_bkg;
	public int m_width;
	public int m_height;
	public PrivacySetting m_privacy;
	public bool m_checkGuardStone;
	public bool m_autoDestroyEmpty;
	public DropTable m_defaultItems;
	public GameObject m_open;
	public GameObject m_closed;
	public EffectList m_openEffects;
	public EffectList m_closeEffects;
	public ZNetView m_rootObjectOverride;
	public Vagon m_wagon;
	public GameObject m_destroyedLootPrefab;
}
