using UnityEngine;

public class GameCamera : MonoBehaviour
{
	public Vector3 m_3rdOffset;
	public Vector3 m_3rdCombatOffset;
	public Vector3 m_fpsOffset;
	public float m_flyingDistance;
	public LayerMask m_blockCameraMask;
	public float m_minDistance;
	public float m_maxDistance;
	public float m_maxDistanceBoat;
	public float m_raycastWidth;
	public bool m_smoothYTilt;
	public float m_zoomSens;
	public float m_inventoryOffset;
	public float m_nearClipPlaneMin;
	public float m_nearClipPlaneMax;
	public float m_fov;
	public float m_freeFlyMinFov;
	public float m_freeFlyMaxFov;
	public float m_tiltSmoothnessShipMin;
	public float m_tiltSmoothnessShipMax;
	public float m_shakeFreq;
	public float m_shakeMovement;
	public float m_smoothness;
	public float m_minWaterDistance;
	public Camera m_skyCamera;
}
