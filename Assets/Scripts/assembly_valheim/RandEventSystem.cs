using UnityEngine;
using System.Collections.Generic;

public class RandEventSystem : MonoBehaviour
{
	public float m_eventIntervalMin;
	public float m_eventChance;
	public float m_randomEventRange;
	public List<RandomEvent> m_events;
}
