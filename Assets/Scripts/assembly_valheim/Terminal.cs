using UnityEngine;
using UnityEngine.UI;

public class Terminal : MonoBehaviour
{
	public RectTransform m_chatWindow;
	public Text m_output;
	public InputField m_input;
}
