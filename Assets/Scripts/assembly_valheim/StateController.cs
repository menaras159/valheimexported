using UnityEngine;

public class StateController : StateMachineBehaviour
{
	public string m_effectJoint;
	public EffectList m_enterEffect;
	public bool m_enterDisableChildren;
	public bool m_enterEnableChildren;
	public GameObject[] m_enterDisable;
	public GameObject[] m_enterEnable;
}
