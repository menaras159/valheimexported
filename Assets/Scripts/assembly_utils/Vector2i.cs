using System;
using UnityEngine;

public struct Vector2i
{
	public Vector2i(Vector2 v) : this()
	{
	}

	public int x;
	public int y;
}
