using UnityEngine;

namespace LuxParticles.Demo
{
	public class LuxParticles_ExtendedFlycam : MonoBehaviour
	{
		public float cameraSensitivity;
		public float climbSpeed;
		public float normalMoveSpeed;
		public float slowMoveFactor;
		public float fastMoveFactor;
	}
}
