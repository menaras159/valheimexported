using UnityEngine;

public class UITooltip : MonoBehaviour
{
	public GameObject m_tooltipPrefab;
	public string m_text;
	public string m_topic;
	public GameObject m_gamepadFocusObject;
}
