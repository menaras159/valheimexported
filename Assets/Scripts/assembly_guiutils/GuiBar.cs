using UnityEngine;

public class GuiBar : MonoBehaviour
{
	public RectTransform m_bar;
	public bool m_smoothDrain;
	public bool m_smoothFill;
	public float m_smoothSpeed;
	public float m_changeDelay;
}
