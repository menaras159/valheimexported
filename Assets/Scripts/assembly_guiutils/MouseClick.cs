using UnityEngine;
using UnityEngine.Events;

public class MouseClick : MonoBehaviour
{
	public UnityEvent m_leftClick;
	public UnityEvent m_middleClick;
	public UnityEvent m_rightClick;
}
