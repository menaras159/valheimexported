using UnityEngine;

public class UIGroupHandler : MonoBehaviour
{
	public GameObject m_defaultElement;
	public GameObject m_enableWhenActiveAndGamepad;
	public int m_groupPriority;
}
