using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;

public class TabHandler : MonoBehaviour
{
	[Serializable]
	public class Tab
	{
		public Button m_button;
		public RectTransform m_page;
		public bool m_default;
		public UnityEvent m_onClick;
	}

	public bool m_gamepadInput;
	public List<TabHandler.Tab> m_tabs;
}
