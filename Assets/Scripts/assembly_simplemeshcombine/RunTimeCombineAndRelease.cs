using UnityEngine;

public class RunTimeCombineAndRelease : MonoBehaviour
{
	public SimpleMeshCombine simpleMeshCombine;
	public float combineTime;
	public float releaseTime;
}
